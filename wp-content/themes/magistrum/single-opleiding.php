<?php get_header(); ?>
	
		
	<!-- Start Page content -->
	<section class="row-fluid page edu-detail">			
		<div class="container">
			<?php while(have_posts()): the_post(); ?>
			<?php $course_id = get_the_ID(); ?>

				<h1 class="pagetitle col-xs-12 col-sm-12 col-md-12 no-padding"><?php the_title(); ?></h1>


				<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-left no-padding-xs margin-bottom-30">

					<span class="col-xs-12 teaser no-padding">
						<?php the_field( 'opleiding_intro' ); ?>
					</span>

					<div class="panel-group" id="information" role="tablist" aria-multiselectable="true">

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading1">
								 <h4 class="panel-title">
									 <a data-toggle="collapse" data-parent="#information" href="#1" aria-expanded="true" aria-controls="1" class="">
										Opleiding
									 </a>
								 </h4>
							</div>

							<div id="1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
								<div class="panel-body">
									<?php the_field( 'opleiding_algemeen' ); ?>
								</div>
							 </div>
						</div>

						<!-- Info Panel -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading2">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#2" aria-expanded="false" aria-controls="2" class="collapsed">
										Programma
									</a>
								</h4>
							</div>
							<div id="2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
								<div class="panel-body">
									<?php the_field( 'programma_algemeen' ); ?>
								</div>
							 </div>
						</div>

						<!-- Info Panel -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading3">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#3" aria-expanded="false" aria-controls="3" class="">
										Resultaat en diploma
									</a>
								</h4>
							</div>
							<div id="3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
								<div class="panel-body">
									<?php the_field( 'resultaat_algemeen' ); ?>
									<?php if( have_rows('beroepen') ): ?>

										<h2>Potentiele beroepen</h2>
										<ul class="professions">

											<?php while ( have_rows('beroepen') ) : the_row(); ?>

												<li>
													<span class="title"><?php the_sub_field('beroep_naam'); ?></span>
													<span class="description"><?php the_sub_field('beroep_omschrijving'); ?></span>
												</li>

											<?php endwhile; ?>

										</ul>
									<?php endif; ?>
								</div>
							</div>
						</div>

						<!-- Info Panel -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading4">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#4" aria-expanded="false" aria-controls="4" class="collapsed">
										Start en locaties
									</a>
								</h4>
							</div>
							<div id="4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
								<div class="panel-body">
									<?php
									the_field( 'start_en_locaties_algemeen' );

									$locations = get_field( 'opleiding_locaties' );

									if( $locations != '' ): ?>
										<h2>Locaties</h2>

										<?php foreach($locations as $location) : ?>

										<?php
										$phone = get_field( 'locatie_telefoonnummer', $location->ID );
										$address = get_field( 'locatie_straatnaam', $location->ID );
										$address .= get_field( 'locatie_huisnummer', $location->ID );
										$zip = get_field( 'locatie_postcode', $location->ID );
										?>

											<span class="location">
												<strong> <i class="fa fa-map-marker"></i>&nbsp;<?php echo $location->post_title; ?>, <?php echo $phone ?></strong>
												<?php echo $address .', ' . $zip; ?>
												<a href="https://www.google.nl/maps/place/<?php the_field('locatie_straatnaam', $location->ID); ?>++<?php echo the_field('locatie_huisnummer', $location->ID); ?>,<?php the_field('locatie_plaats' ,$location->ID); ?>" target="_blank">Routebeschrijving</a>
											</span>
										<?php endforeach;
									endif;
									?>

								</div>
							</div>
						</div>

						<!-- Info Panel -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading5">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#5" aria-expanded="true" aria-controls="5" class="collapsed">
										Investering
									</a>
								</h4>
							</div>
							<div id="5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
								<div class="panel-body">

									<?php the_field( 'investering_algemeen' ); ?>

								</div>
							</div>
						</div>

						<!-- Info Panel -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading6">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#6" aria-expanded="true" aria-controls="6" class="collapsed">
										Toelating
									</a>
								</h4>
							</div>
							<div id="6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
								<div class="panel-body">
									<?php the_field('toelating_algemeen'); ?>
								</div>
							</div>
						</div>

						<!-- Custom info tab -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading7">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#7" aria-expanded="false" aria-controls="7" class="collapsed">
										Contact
									</a>
								</h4>
							</div>
							<div id="7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
								<div class="panel-body">
									<?php the_field('contact_algemeen'); ?>
								</div>
							</div>
						</div>

						<!-- Custom info tab -->
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading8">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#information" href="#8" aria-expanded="false" aria-controls="8" class="collapsed">
										Studievoorlichting
									</a>
								</h4>
							</div>
							<div id="8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
								<div class="panel-body">
									<?php the_field('studievoorlichting_algemeen'); ?>
								</div>
							</div>
						</div>

					</div><!-- End Panel-group -->

				<!-- End Col-md-8 -->
				</div>
			<?php endwhile; ?>

			<aside class="col-xs-12 col-sm-12 col-md-3 pull-right sidebar scroll-sidebar no-padding-xs">
				<?php if ( is_active_sidebar( 'course_widget_links' ) ) : ?>
					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<?php dynamic_sidebar( 'course_widget_links' ); ?>
					</div>
				<?php endif; ?>
				
				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<?php if ( is_active_sidebar( 'image_widget' ) ) : ?>
						<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
							<?php dynamic_sidebar( 'image_widget' ); ?>
						</div>
					<?php endif; ?>
				</div>
				<?php
				if(get_field('registration_image_bool')) : ?>
					<?php $image = get_field('registration_image'); ?>

					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<img class="img-responsive" src="<?php echo $image['sizes']['medium'] ?>" />
					</div>

				<?php endif; ?>

			</aside>
					
		<!-- ./ End Main Container -->	
		</div>
	<!-- ./ End Section page content -->
	</section>
		
	<!-- Secundary Information -->
	<section class="secundary">
		<div class="container">
			<span class="heading">Meer opleidingen bij Magistrum:</span>
			<div class="col-xs-12 no-padding-xs col-sm-12 col-md-8 page-sec-content no-padding-left">
				<div class="edu col-xs-12">
					<ul class="educations col-xs-12 no-padding">
						<?php
						$args = array(
							'post_type' => 'opleiding',
							'posts_per_page' => 3,
							'orderby' => 'rand',
							'post__not_in' => array($course_id)
						);
						$courses = new WP_Query($args);
						if($courses->have_posts()) : while($courses->have_posts()) : $courses->the_post();
							?>

							<li>
								<h3><a href="<?php the_permalink(); ?>"><i class="fa fa-graduation-cap"></i>&nbsp;<?php the_title(); ?></a></h3>

							<span class="col-xs-12 col-sm-8 col-md-10 no-padding-left intro">
								<p><?php the_field('opleiding_intro'); ?></p>
							</span>

								<a class="col-xs-12 col-sm-4" href="<?php the_permalink(); ?>">
									<span class="hidden-lg">Bekijk deze opleiding</span>
									<i class="fa fa-angle-right"></i>
								</a>
							</li>

						<?php endwhile;?>
						<?php endif; ?>

					</ul>
					<!-- / End Education List -->
				</div>
				<!-- / End .edu -->
			</div>


		</div>
	</section>
	

<?php get_footer(); ?>