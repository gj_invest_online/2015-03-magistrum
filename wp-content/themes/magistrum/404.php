<?php get_header(); ?>

    <section class="page content-page row-fluid">
        <div class="container">

            <div itemscope itemtype="http://www.schema.org/Article" class="col-xs-12 col-sm-12 col-md-8 pull-right page-content no-padding-right no-padding-xs margin-bottom-30">

                <span itemprop="headline" content="404 - Pagina kan niet worden gevonden"></span>

                <h1 class="pagetitle col-xs-12 no-padding">404 - Pagina kan niet worden gevonden</h1>

                <div itemprop="articleBody">
                    <p>De door u opgevraagde pagina kan niet worden gevonden.</p>
                </div>

            </div>

            <aside class="col-xs-12 col-sm-12 col-md-3 pull-left sidebar no-padding-xs">
                <div class="widget-container">
                    <div>
                        <div>
                            <!-- Contact -->
                            <?php if ( is_active_sidebar( 'io_contact_widget' ) ) : ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                    <?php dynamic_sidebar( 'io_contact_widget' ); ?>
                                </div>
                            <?php endif; ?>
                            <!-- End Contact -->  </div>
                        <div>
                            <!-- news and events -->
                            <?php if ( is_active_sidebar( 'magazine_news_widget' ) ) : ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                    <?php dynamic_sidebar( 'magazine_news_widget' ); ?>
                                </div>
                            <?php endif; ?>
                            <!-- / End news and events -->  </div>
                    </div>
                </div>

            </aside>

        </div>

    </section>

<?php get_footer(); ?>