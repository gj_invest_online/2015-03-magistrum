<?php
/*
Template Name: Contact opnemen
*/
?>

<?php get_header(); ?>

    <!-- Start Page Content Section -->
    <section class="page content-page row-fluid">
        <div class="container">


            <!-- Start col-md-8 -->
            <div class="col-xs-12 col-sm-12 col-md-8 pull-right page-content no-padding-right no-padding-xs margin-bottom-30">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <h1 class="pagetitle col-xs-12 no-padding"><?php the_title(); ?></h1>

                    <?php the_content(); ?>
                <?php endwhile; endif; ?>

                <iframe src="http://magistrum.force.com/ContactFormulier" style="width: 100%; height: 451px;"></iframe>

            </div>

            <aside class="col-xs-12 col-sm-12 col-md-3 pull-left sidebar no-padding-xs">

                <div class="widget-container">

                    <div>
                        <div>
                            <!-- Contact -->
                            <?php if ( is_active_sidebar( 'io_contact_widget' ) ) : ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                    <?php dynamic_sidebar( 'io_contact_widget' ); ?>
                                </div>
                            <?php endif; ?>
                            <!-- End Contact -->
                        </div>
                        <div>
                            <!-- news and events -->
                            <?php if ( is_active_sidebar( 'magazine_news_widget' ) ) : ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                                    <?php dynamic_sidebar( 'magazine_news_widget' ); ?>
                                </div>
                            <?php endif; ?>
                            <!-- / End news and events -->
                        </div>
                    </div>
                </div>

            </aside>

        </div>

    </section>

<?php get_footer(); ?>