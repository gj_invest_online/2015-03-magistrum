<?php get_header(); ?>

	<!-- Start Page Content Section -->
	<section class="page news-events row-fluid">
		<div class="container">

			<!-- Start col-md-8 -->
			<div class="col-xs-12 col-sm-12 col-md-7 page-content no-padding-left no-padding-xs margin-bottom-30">
				<h1 class="pagetitle col-xs-12 no-padding">Nieuws</h1>

				<?php if(have_posts()): while(have_posts()): the_post(); ?>

					<article class="col-xs-12 no-padding margin-bottom-40 news-item">
						<figure class="col-xs-12 col-sm-3 col-md-2 col-lg-3">
							<?php the_post_thumbnail(); ?>
						</figure>
						<div class="body col-xs-12 col-sm-9 col-md-9 col-lg-9">
							<h2>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<small class="meta">28 januari 2015, door Avans Deeltijd</small>
							</h2>
							<?php the_excerpt(); ?>
							<a class="more-link" href="<?php the_permalink(); ?>">Lees verder <i class="fa fa-angle-right"></i></a>
						</div>
					</article>

				<?php endwhile; ?>
				<?php endif; ?>

				<hr class="col-xs-12 no-padding" />
				<!-- Pagination -->
				<?php get_template_part('templates/pagination'); ?>

			</div>
			<!-- Events -->
			<div class="col-xs-12 col-sm-12 col-md-5 pull-right events no-padding-xs no-padding-right margin-bottom-50">
				<h1 class="pagetitle col-xs-12 no-padding">Events</h1>
				
				<?php
				// find todays date
				$date = date('Ymd');
				
				// args
				$args = array(
					'numberposts'		=> -1,
					'post_type'			=> 'event',
					'posts_per_page'	=> 6,
					'paged'				=> $paged,
					'orderby'			=> 'meta_value_num',
					'order'				=> 'ASC',
					'meta_query'		=> array(
						array(
							'key'			=> 'io_event_date',
							'compare'		=> '>=',
							'value'			=> $date,
						),
					),
				);
				
				$event_query = new WP_Query($args);
				
				if($event_query->have_posts()) :
					$count = 1;
					
					while( $event_query->have_posts() ) : $event_query->the_post();
					
						// Date and Time
						$unixtimestamp = strtotime(get_field('io_event_date'));
						$month = date_i18n('F', $unixtimestamp);
						$day = date_i18n('d', $unixtimestamp);
						
						// The Location
						if(get_field('event_location') != null) :
							$location_obj = get_field('event_location');
							$phone_num = get_field('locatie_telefoonnummer', $location_obj[0]->ID);
							$location = $location_obj[0]->post_title;
						else :
							$phone_num = '076 - 525 88 96';
							$location = 'Onbekend';
						endif;
						 
						if ($count <=3) : ?>
							
							<article class="col-xs-12 col-sm-11 col-md-10 col-lg-10 margin-bottom-40 event-item">
								<span class="date cta-grey">
									<?php echo $day; ?> <small><?php echo $month; ?></small>
								</span>
								<h2>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									<small><?php the_field('io_event_start'); ?> – <?php the_field('io_event_end') ?>, <?php echo $location; ?></small>
								</h2>
								<div class="body">
									<?php the_excerpt(); ?>
									<a href="<?php the_permalink(); ?>" class="more-link">Lees verder <i class="fa fa-angle-right"></i></a>
								</div>
							</article>
							
							<?php $count ++; ?>
							
						<?php else : ?>
							
							<article class="col-xs-12 col-sm-11 col-md-10 col-lg-12 margin-bottom-20 event-item compact">
								<a href="<?php the_permalink(); ?>">
									<span class="date cta-grey col-xs-2 col-lg-2 col-md-2">
										<?php echo $day; ?> <small><?php echo $month; ?></small>
									</span>
									<h2 class="col-xs-10 col-xs-offset-2 col-md-12 col-lg-10">
										<?php the_title(); ?>
										<small><?php the_field('io_event_start'); ?> – <?php the_field('io_event_end') ?>, <?php echo $location; ?></small>
									</h2>
									<i class="fa fa-angle-right"></i>
								</a>
							</article>					
							
						<?php endif; ?>
			
					
					<?php endwhile; ?>
				<?php endif; ?>
				
			</div>
		</div>
	</section>
<?php get_footer(); ?>