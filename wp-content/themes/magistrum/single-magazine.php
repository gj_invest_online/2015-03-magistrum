<?php get_header(); ?>

	<!-- Start Page Content Section -->
	<section class="page magazine-overview grey-bg row-fluid">
		<div class="container">
			<!-- Start col-md-8 -->
			<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-left no-padding-xs margin-bottom-30">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article class="col-xs-12 no-padding margin-bottom-40 magazine-item single-item">
					<figure>
						<?php the_post_thumbnail(); ?>
					</figure>
					<h3>
						<?php the_title(); ?>
						<small><?php the_time('j F Y, H:i'); ?></small>
						<i class="fa fa-angle-right"></i>
					</h3>
					<div>
						<?php the_content(); ?>
					</div>
					<footer>
						<div class="pull-left">
							<i class="fa fa-tag"></i> <?php the_tags(); ?>
						</div>
						<div class="pull-right">
							<?php echo wpfai_social(); ?>
						</div>
					</footer>
				</article>

				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, we hebben de pagina niet gevonden.' ); ?></p>
				<?php endif; ?>

			</div>
		</div>
	</section>



		
<?php get_footer(); ?>