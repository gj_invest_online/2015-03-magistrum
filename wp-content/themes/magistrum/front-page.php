<?php get_header(); ?>

	<!-- Start Main container -->
	<div class="container">
		<div class="widget-container">
			<div>

				<div>
					<?php get_template_part('templates/searchform', 'alternate'); ?>
				</div>

				<div>
					<!-- Spotlight -->
					<section class="spotlight default">
						<!-- Blok 1 -->
						<div class="item video rs-1 col-xs-12 col-sm-12 col-md-5 no-padding hidden-xs">
							<?php if(get_field('clip_or_image', 'option') == 'Clip') :

								the_field('youtube_clip', 'option');
							elseif(get_field('clip_or_image', 'option') == 'Afbeelding') : ?>
								<img src="<?php the_field('image_block_1', 'option'); ?>">
							<?php else : ?>
								<iframe width="560" height="315" src="https://www.youtube.com/embed/FgqxxO-pyAo?controls=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
							<?php endif; ?>
						</div>

						<!-- Blok 2 -->
						<div class="item article rs-1 col-xs-12 col-sm-7 col-md-4">
							<article>
								<h2><?php the_field('header_blok2', 'option'); ?></h2>
								<p><?php the_field('tekst_blok2', 'option'); ?></p>
								<a href="<?php the_field('link_blok2', 'option'); ?>" class="more-link">
									<?php the_field('link_tekst_blok2', 'option'); ?> <i class="fa fa-angle-right"></i>
									<hr/>
								</a>
							</article>
						</div>

						<!-- Blok 3 -->
						<div class="item testimonial rs-2 col-xs-12 col-sm-5 col-md-3 no-padding pull-right hidden-xs">
							<?php
							$link = get_field('homepage_testimonial', 'option');
							$testimonial = get_post( $link[0] );
							$testimonial_link = get_permalink($testimonial->ID);
							$testimonial_image = wp_get_attachment_image_src( get_post_thumbnail_id( $link[0] ), 'medium' );
							$course_id = get_field('linked_opleiding', $link[0]);
							$course_title = get_the_title( $course_id[0] );

							?>
							
							<div class="mask">
								<img src="<?php echo $testimonial_image[0]; ?>" alt="Mona Wells" />
							</div>
							<span>
								<span class="name"><?php echo $testimonial->post_title; ?></span>
								<span class="study"><?php echo $course_title; ?></span>
								<blockquote class="hidden-xs hidden-sm">
									<?php echo $testimonial->post_excerpt; ?>
								</blockquote>
								<hr class="col-xs-12 visible-xs no-padding" />
								<a href="<?php echo $testimonial_link; ?>" class="more-link">
									Lees het verhaal van <?php echo $testimonial->post_title; ?> <i class="fa fa-angle-right"></i>
									<hr/>
								</a>
							</span>
						</div>

						<!-- Blok 4 -->
						<div class="item blogpost col-xs-12 col-sm-6 col-md-5 col-md-5-5 rs-1 hidden-xs">

							<?php
							$args = array(
								'post_type' => 'magazine',
								'numberposts' => 1,
								'tax_query' => array(
									array(
										'taxonomy' => 'post_format',
										'field' => 'slug',
										'terms' => 'post-format-video',
										'operator' => 'NOT IN'
									))
							);
							$latest_mag = wp_get_recent_posts( $args, OBJECT );
							$mag_link = get_the_permalink($latest_mag[0]->ID);
							$date = get_the_date('', $latest_mag[0]->ID);
							$unixtimestamp = strtotime($date);
							$month = date_i18n('F', $unixtimestamp);
							$day = date_i18n('d', $unixtimestamp);
							?>

							<aside class="hidden-xs">
								<span>MAGAZINE</span>
								<span class="date">
									<small><?php echo $month; ?></small>
									<span><?php echo $day; ?></span>
								</span>
							</aside>
							<h3><?php echo $latest_mag[0]->post_title; ?></h3>
							<p><?php echo $latest_mag[0]->post_excerpt; ?></p>
							<a href="<?php echo $mag_link; ?>" class="more-link">
								lees verder <i class="fa fa-angle-right"></i>
								<hr/>
							</a>
						</div>

						<!-- Blok 5 -->
						<div class="item photo col-md-3 col-sm-6 col-md-3-5 rs-1 hidden-xs">
							<img src="<?php the_field('homepage_afbeelding', 'option'); ?>" />
						</div>

					</section>
					<!-- / End Spotlight -->
				</div>

				<!-- Section Divider -->
				<div>
					<hr class="col-xs-12 no-padding" />
				</div>

				<!-- Toptaken -->

				<div>
					<div>
						<div>
							<section class="ctas">
								<span class="subheading col-xs-12 no-padding"><?php //the_field('call_to_action_titel', 'option'); ?></span>
								<?php

								$counter = 1;

								while($counter < 5 ) {
									$cta_icon = get_field('icon_cta_' . $counter, 'option');
									$cta_title = get_field('cta_titel_' . $counter, 'option');
									$cta_sub = get_field('cta_subtitel_' . $counter, 'option');
									$cta_link = get_field('cta_link_' . $counter, 'option');
									?>

									<div class="col-xs-12 col-sm-12 col-md-3">
										<a href="<?php echo $cta_link; ?>" class="cta cta-default">
											<div class="col-xs-2"><?php echo $cta_icon; ?></div>
											<div class="col-xs-10">
												<span><?php echo $cta_title; ?></span>
												<small><?php echo $cta_sub; ?></small>
											</div>
											<i class="fa fa-angle-right"></i>
										</a>
									</div>

									<?php
									$counter ++;
								}

								?>

							</section>
						</div>
					</div>
					<!-- End toptaken -->
				</div>

				<!-- Section Divider -->
				<div>
					<hr class="col-xs-12 no-padding" />
				</div>

				<!-- Clusters -->
				<div>
					<section class="clusters">

						<span class="subheading col-xs-12 no-padding">Magistrum opleidingen:</span>

						<?php

						$args = array(
							'post_type' => 'opleiding',
							'posts_per_page' => -1,
							'orderby' => 'menu_order',
							'order' => 'ASC'
						);

						$query = new WP_Query( $args );

						if ( $query->have_posts() ) : ?>

							<?php while ( $query->have_posts() ) : $query->the_post(); ?>
								<div class="cluster col-sm-6">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
										<div class="spec">
											<h3><div class="table-cell"><?php the_title(); ?> <i class="fa fa-angle-right"></i></div></h3>
										</div>
									</a>
								</div>
							<?php endwhile; ?>

							<?php wp_reset_postdata(); ?>

						<?php endif; ?>

					</section>
				</div>
				<!-- End Clusters -->

			<!-- End Wrapper DIV -->
			</div>
		<!-- End widget container -->
		</div>
	<!-- ./ End Main Container -->
	</div>


	<!-- Secundary Information -->
	<section class="secundary">
		<div class="container">
			<!-- Nieuws -->
			<div class="col-md-4 col-sm-6 col-xs-12 no-padding-left no-padding-xs">
				<div class="widget-container">

					<div>
						<div>

							<div class="widget widget-default widget-news">
								<span class="widget-heading">Nieuws & Events</span>

								<?php
								$args = array(
									'post_type' => array('post', 'event'),
									'posts_per_page' => 6
								);

								$post_query = new WP_Query($args);
								if($post_query->have_posts()) : while($post_query->have_posts()) : $post_query->the_post(); ?>

									<?php
									// Set variables needed
									$post_type = '';
									$date = '';

									if(get_post_type(get_the_ID()) == 'post') {
										$post_type = 'Nieuws';
										$date = get_the_time('j F Y');
									} else {
										$post_type = 'Event';
										$unixtimestamp = strtotime(get_field('io_event_date'));
										$date = date_i18n('j F Y', $unixtimestamp);
									}
									?>

									<article>
										<div class="mask">
											<?php the_post_thumbnail(); ?>
										</div>
										<div class="msg">
											<span class="meta"><?php echo $post_type ?> | <?php echo $date; ?></span>
											<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
										</div>
									</article>

								<?php endwhile; ?>
								<?php else : ?>
									<article>
										<div class="mask">

										</div>
										<div class="msg">
											<span class="meta">Nieuws | </span>
											<a class="title">Geen nieuws gevonden</a>
										</div>
									</article>
								<?php endif; ?>

								<a href="<?php echo get_post_type_archive_link( 'faq' ); ?>" class="more-link">
									Meer Nieuws & Evenementen <i class="fa fa-angle-right"></i>
									<hr/>
								</a>
							</div>
							<!-- End Widget -->

						</div>
					</div>

				</div>
				<!-- End widget container -->
			</div>

			<!-- FAQ -->
			<div class="col-md-4 col-sm-6 col-xs-12 no-padding-xs">
				<div class="widget-container">

					<div>
						<div>

							<div class="widget widget-default widget-faq">
								<span class="widget-heading">Veelgestelde vragen</span>
								<?php
								$args = array(
									'post_type' => 'faq',
									'posts_per_page' => 6,
									'orderby' => 'rand'
								);

								$faq_query = new WP_Query( $args ); ?>

								<?php if ( $faq_query->have_posts() ) : ?>

									<!-- the loop -->
									<?php while ( $faq_query->have_posts() ) : $faq_query->the_post();
										$terms = get_the_terms(get_the_ID(), 'onderwerp');
							
										if ( $terms && ! is_wp_error( $terms ) ) :
											foreach($terms as $term) :
												if($term->name) :
													$cat_name = $term->name;
												endif;
											endforeach;
										else :
											$cat_name = 'vraag';
										endif;
										?>
										<article>
											<h4><?php echo $cat_name; ?></h4>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?><i class="fa fa-angle-right"></i></a>
										</article>
									<?php endwhile; ?>

									<?php wp_reset_postdata(); ?>

								<?php else : ?>
									<h4>Geen vragen gevonden</h4>
								<?php endif; ?>

								<a href="<?php echo get_post_type_archive_link( 'faq' ); ?>" class="more-link">
									Ik heb een andere vraag <i class="fa fa-angle-right"></i>
									<hr/>
								</a>
							</div>
							<!-- End Widget -->
						</div>
					</div>
				</div>
				<!-- End widget container -->
			</div>

			<!-- Locations -->
			<div class="col-md-4 col-sm-12 col-xs-12 no-padding-left-sm no-padding-right no-padding-xs">
				<div class="widget-container">

					<div>
						<div>

							<div class="widget widget-default widget-contact">
								<div class="col-md-12 col-sm-6 col-xs-12 no-padding">
									<span class="widget-heading">Contact</span>
									<?php
									get_field('partners', 'option');
									if( have_rows('partners', 'option') ): while( have_rows('partners', 'option') ): the_row(); ?>

										<span class="location">
											<strong><i class="fa fa-map-marker fa-fw"></i><?php the_sub_field('partner_naam', 'option'); ?></strong>
											<strong class="phone-num"><i class="fa fa-fw"></i><?php the_sub_field('partner_telefoonnummer', 'option'); ?></strong>
										</span>

									<?php endwhile; ?>
									<?php else : ?>

										<span class="location">
											<strong><i class="fa fa-map-marker"></i>&nbsp;Geen partner geselecteerd</strong>
										</span>

									<?php endif; ?>
								</div>
								
								<?php
								$form = get_post(211);
								if(!empty($form)) : ?>
									<hr class="col-xs-12 no-padding hidden-sm"/>
									<span class="widget-heading"><?php echo get_the_title(211); ?></span>
                					<?php echo do_shortcode( '[contact-form-7 id="211" title="Wij bellen u"]' ); ?>
								<?php endif; ?>

							</div>
							<!-- End widget -->
						</div>
					</div>

				</div>
				<!-- End widget container -->
			</div>

		</div>
	</section>

<?php get_footer(); ?>