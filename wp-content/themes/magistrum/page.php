<?php get_header(); ?>

    <!-- Start Page Content Section -->
    <section class="page content-page row-fluid">
        <div class="container">


            <!-- Start col-md-8 -->
            <div class="col-xs-12 col-sm-12 col-md-8 pull-right page-content no-padding-right no-padding-xs margin-bottom-30">

                <div class="btn-group col-xs-12 col-sm-6 col-md-3 no-padding-left hidden-md hidden-lg">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Over Magistrum <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <?php mg_about_menu_mobile(); ?>
                    </ul>
                </div>

                <?php while ( have_posts() ) : the_post(); ?>

                    <hr class="col-xs-12 hidden-md hidden-lg no-padding" />

                    <h1 class="pagetitle col-xs-12 no-padding"><?php the_title(); ?></h1>
                    
                  	<?php the_content(); ?>

                <?php endwhile; ?>
                <!-- End Col-md-8 -->
            </div>

            <aside class="col-xs-12 col-sm-12 col-md-3 pull-left sidebar no-padding-xs">

                <div class="sidemenu no-padding hidden-xs hidden-sm">
					
                    <?php mg_about_menu_items() ?>

                </div>

                <?php if ( is_active_sidebar( 'image_widget' ) ) : ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                      <?php dynamic_sidebar( 'image_widget' ); ?>
                    </div>
                <?php endif; ?>

            </aside>

            <!-- ./ End Main Container -->
        </div>
        <!-- ./ End Section page content -->
    </section>

<?php get_footer(); ?>