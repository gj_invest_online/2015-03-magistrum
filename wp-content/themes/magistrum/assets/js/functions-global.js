// CLUSTER FADES
var alfades = '.clusters .cluster, .faq-popular li, .testimonial-overview .testimonial';

 $(alfades).on('mouseenter', function() {
	 $(alfades).css('opacity','0.6');
	 $(this).css('opacity','1');
	 
	 $(this).on('mouseleave', function(){
		$(alfades).css('opacity','1'); 
	 });
	 
 });
 

// Searchfield behavior //
$('.btn-search').on('click', function() {
	$('.pagesearch').slideToggle();
	$('.pagesearch input').focus();	
	$(this).toggleClass('active');
	$(this).children('i').toggleClass('fa-search');
	$(this).children('i').toggleClass('fa-times-circle');	
});
// PANEL Behavior //

$('.panel-title').on('click', function() {
	if( $(this).hasClass('active') ) {
		$(this).removeClass('active');
		$(this).children('a').children('small').text('Toon vragen');
	} else {
		$('.panel-title').removeClass('active');
		$('.panel-title').children('a').children('small').text('Toon vragen');		
		$(this).addClass('active');		
		$(this).children('a').children('small').text('Verberg vragen');		
	}
});


$('.panel-collapse.in').parent().children('.panel-heading').children('h4').addClass('active');


$('.more-text').on('click', function() {
	$(this).children('i').toggleClass('fa-caret-down');
	$(this).children('i').toggleClass('fa-caret-right');

	$(this).next('span').slideToggle();
});

// 16:9 Ratio for Iframes on magazine page
function videoRatio() {
	var iWidth = $('iframe').outerWidth();
	$('iframe').attr('height', + iWidth *9/16);
}
videoRatio();

// 1:1 Ratio for Iframes on magazine page
function squareRatio() {
	var square = $('.clusters .cluster').outerWidth();
	var compact = $('.compact').outerWidth();	
	
	$('.clusters .cluster').css('height', + square +'px');
}

// Equal Height for widget elements homepage 
function equalHeight() {

	var screen = $(document).width() + 15;
	var newsHeight = $('.widget-news').outerHeight();
	var faqHeight = $('.widget-faq').outerHeight();	
	
	if(screen > 768 && screen < 1024) {
		if( newsHeight > faqHeight ) {
			$('.widget-faq').height(newsHeight);
		} else {
			$('.widget-news').height(faqHeight);
		};	
	} else {
		$('.widget').removeAttr('style');
	}

	   
}
equalHeight();

// BLOCKSIT

function initBlock() {
	$(window).load( function() {		
		var screen = $(document).width() + 15;	
		if(screen > 768 && screen < 1024) {
			$('.sidebar').BlocksIt({
				numOfCol: 2,
				offsetX: 8,
				offsetY: 8,
				blockElement: '.sidebar > .col-xs-12'
			});
		} else {
			$('.sidebar').removeAttr('style');
			$('.sidebar > .col-xs-12').removeAttr('style');
		}
		
	});		
}
initBlock();


function resizeBlock() {
	var screen = $(document).width() + 15;	
	if(screen > 768 && screen < 1024) {
		$('.sidebar').BlocksIt({
			numOfCol: 2,
			offsetX: 8,
			offsetY: 8,
			blockElement: '.sidebar > .col-xs-12'
		});

	} 
	if(screen < 768) {
		$('.sidebar').removeAttr('style');
		$('.sidebar > .col-xs-12').removeAttr('style');
	}
			
}// End Function


// Sidemenu foldout 
$('.sidemenu nav > li > a').on('click', function(event) {

	var link = $(this).attr('href');
	if( link == '' ){
		event.preventDefault();
		$(this).closest('li').children('ul').slideToggle();		
	}	
});

// Filters on Testimonial page
$('.phone-filters').on('click', function(){
	$('.phone-filters .filters').slideToggle();
});	

$('.phone-filters .filter').on('click', function(){
	var contents = $(this).text();
	$('.phone-filters > span').html(contents + '&nbsp;<i class="fa fa-caret-down"></i>');
});	

$(window).resize(function(){
	squareRatio();
	videoRatio();
	resizeBlock();
});

//enable the Testimonial overview
$('.page-content').mixItUp();


// modifying jquery validator to conform to bootstrap 3
/*
$('#advice-form').validator({
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            error.insertAfter(element.parent().parent());
        } else if (element.hasClass( "date" )){
        	error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});
*/
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: NL
 */
/*
jQuery.extend(jQuery.validator.messages, {
        required: "Dit is een verplicht veld.",
        remote: "Controleer dit veld.",
        email: "Vul hier een geldig e-mailadres in.",
        url: "Vul hier een geldige URL in.",
        date: "Vul hier een geldige datum in.",
        dateISO: "Vul hier een geldige datum in (ISO-formaat).",
        number: "Vul hier een geldig getal in.",
        digits: "Vul hier alleen getallen in.",
        creditcard: "Vul hier een geldig creditcardnummer in.",
        equalTo: "Vul hier dezelfde waarde in.",
        accept: "Vul hier een waarde in met een geldige extensie.",
        maxlength: jQuery.validator.format("Vul hier maximaal {0} tekens in."),
        minlength: jQuery.validator.format("Vul hier minimaal {0} tekens in."),
        rangelength: jQuery.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1} tekens."),
        range: jQuery.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1}."),
        max: jQuery.validator.format("Vul hier een waarde in kleiner dan of gelijk aan {0}."),
        min: jQuery.validator.format("Vul hier een waarde in groter dan of gelijk aan {0}.")
});
*/
// initialize the Testimonial overview

$( document ).ready(function() {
    $("#we-call-you").submit(function() {
        var url = $(this).attr('data-action'); 

        $.ajax({
               type: "get",
               url: url,
               data: $("#we-call-you").serialize(), 
               headers: { 
                   Accept : "text/html; charset=utf-8",
               },
               success: function(data) {
            	   $("#we-call-you").replaceWith(data);
               }
             });

        return false; 
    });
});