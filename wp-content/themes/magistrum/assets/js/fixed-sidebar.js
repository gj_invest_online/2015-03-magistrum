// FIXED SIDEBAR //
function fix() {

	var screenwidth = $(document).width() + 15;
	if(screenwidth > 1280) {
	
	    var top = $('.sidebar').offset().top - parseFloat($('.sidebar').css('marginTop').replace(/auto/, 0));
	    var footTop = $('footer').offset().top - parseFloat($('footer').css('marginTop').replace(/auto/, 0));
	    var right = $('.sidebar').offset().left, $window = $(window);	
	    
	    
	    var maxY = footTop - $('.sidebar').outerHeight() - 60;
	    
	    
	    
	    
	    
	    $(window).scroll(function(evt) {
		    if($(document).width() + 15 > 1280) {	    
		        var y = $(this).scrollTop();
		        if (y > top) {
		            if (y < maxY) {
		                $('.sidebar').addClass('fixed').removeAttr('style');
		                $('.sidebar').css('left',+ right +'px');
		            } else {
		                $('.sidebar').removeClass('fixed').css({
		                    position: 'absolute',
		                    right: 0,
		                    left: 'inherit',
		                    top: (maxY - top) + 80 +'px'
		                });
		            }
		        } else {
		            $('.sidebar').removeClass('fixed');
		            $('.sidebar').removeAttr('style');	            
		        }
		     }   
	    });
	    
	} 	else {
		$('.sidebar').removeClass('fixed');
		$('.sidebar').removeAttr('style');		
	} // End If

} // End Function



fix();

$(document).resize(function(){

	fix();
	
});