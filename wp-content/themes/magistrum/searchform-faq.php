<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">

    <input type="text" placeholder="<?php echo esc_attr_x( 'Zoeken naar antwoorden', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Zoeken naar antwoorden:', 'label' ) ?>" />
	<input type="hidden" name="post_type" value="faq" />
	<button type="submit"><i class="fa fa-search"></i></button>

</form>