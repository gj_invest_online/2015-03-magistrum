<?php get_header(); ?>

<!-- Start Page Content Section -->
<section class="page edu-overview grey-bg row-fluid">	
	<div class="container">
	
		<div class="row intro-large">
			<div class="col-xs-12 col-sm-7 col-md-8 no-padding-left">
				Hulp nodig bij uw keuze?<br/>Vraag gratis een orientatiegesprek of proefles aan.
			</div>
			
			<aside class="col-xs-12 col-sm-5 col-md-3 pull-right ctas">

			</aside>
			
			<hr class="col-xs-12 no-padding" />
		</div>	
		
		
		<!-- Start col-md-8 -->
		<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-left no-padding-xs margin-bottom-30">
		<?php
		$args = array(
		'post_type' => 'opleiding',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) :

		while ( $query->have_posts() ) : $query->the_post(); ?>

			<article class="edu col-xs-12 col-sm-12 col-md-12">
				<figure class="col-md-4 col-sm-4 compact no-padding-left">
					<?php the_post_thumbnail(); ?>
					<div class="spec">
						<span class="nametag"><?php the_title(); ?></span>							
					</div>
				</figure>

				<div class="edu-intro col-xs-12 col-sm-8 col-md-8 col-md-offset-4 col-sm-offset-4">
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p><?php the_field( 'opleiding_intro' ); ?></p>
					<a href="<?php the_permalink(); ?>" class="btn btn-avans"><i class="fa fa-list"></i>&nbsp;Opleiding tonen</a>
				</div>	
			</article>
			
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, geen opleidingen gevonden' ); ?></p>
			<?php endif; ?>																	
			
		<!-- End Col-md-8 -->
		</div>
		
		<aside class="col-xs-12 col-sm-12 col-md-3 pull-right sidebar no-padding-xs  no-padding-right">
			<?php if ( is_active_sidebar( 'course_widget_links' ) ) : ?>
				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<?php dynamic_sidebar( 'course_widget_links' ); ?>
				</div>
			<?php endif; ?>

			<div class="col-xs-12 col-sm-6 col-md-12 no-padding">
				<?php if ( is_active_sidebar( 'image_widget' ) ) : ?>
					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<?php dynamic_sidebar( 'image_widget' ); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-12 no-padding">
				<?php if ( is_active_sidebar( 'faq_list_widget' ) ) : ?>
					<?php dynamic_sidebar( 'faq_list_widget' ); ?>
				<?php endif; ?>
			</div>

			
		</aside>
		
	<!-- ./ End Main Container -->	
	</div>
<!-- ./ End Section page content -->		
</section>

<?php get_footer(); ?>