<footer>
	
	<?php if(have_rows( 'partners', 'options' )) : ?>
		<div class="container">
			<div class="row partner-logo-row">
				
			<?php
			while(have_rows('partners', 'options')) : the_row(); ?>
			
				<div class="col-xs-12 col-sm-5ths">
					<img class="img-responsive" src="<?php the_sub_field( 'partner_logo', 'options' ); ?>" />
				</div>
				
			<?php endwhile; ?>
			
			</div>
			<hr/>
		</div>
		
	<?php endif; ?>	
		
	
	<div class="container">

		<?php if ( is_active_sidebar( 'footer_column_1' ) ) : ?>
			<?php dynamic_sidebar( 'footer_column_1' ); ?>
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'footer_column_2' ) ) : ?>
			<?php dynamic_sidebar( 'footer_column_2' ); ?>
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'footer_column_3' ) ) : ?>
			<?php dynamic_sidebar( 'footer_column_3' ); ?>
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'footer_column_4' ) ) : ?>`
			<?php dynamic_sidebar( 'footer_column_4' ); ?>
		<?php endif; ?>

				<span class="col-xs-12 no-padding copyright">
					<i class="fa fa-copyright"></i>&nbsp;&nbsp;<?php echo date("Y") ?> Magistrum
				</span>

	</div>
	<?php wp_footer(); ?>
</footer>



<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/plugins/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/plugins/mixitup/jquery.mixitup.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/plugins/validator/validator.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/equalheight.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/functions-global.js"></script>
</body>
</html>