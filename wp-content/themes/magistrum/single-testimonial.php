<?php get_header(); ?>

<!-- Start Page Content Section -->
<section class="page testimonial-detail row-fluid">	
	<div class="container">
		
		<?php while(have_posts()): the_post();
			$linked_course = get_field('linked_opleiding');
			$course_url = get_permalink($linked_course[0]);
			$course_title = get_the_title($linked_course[0]);
		
			?>
			<h1 class="pagetitle col-xs-12 no-padding">
				<?php the_title(); ?>
				<span class="subtitle"><?php echo $course_title; ?></span>
			</h1>
		
			<!-- Start col-md-8 -->
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-7 page-content no-padding-left no-padding-xs margin-bottom-30">
				<?php the_content(); ?>
			</div>
		
			<aside class="col-xs-12 col-sm-12 col-md-4 no-padding-xs no-padding-right pull-right"></aside>
		
		<?php endwhile; ?>	
			
		<div class="row intro-large">
			<hr class="col-xs-12 no-padding" />
			<div class="col-xs-12 col-sm-7 col-md-6 no-padding-left">
				Benieuwd naar de opleiding die <?php the_title(); ?> heeft gevolgd?
			</div>
								
			<aside class="col-xs-12 col-sm-5 col-md-4 pull-right no-padding-right ctas">
				<a href="<?php echo $course_url; ?>" class="cta cta-red">
					<span>Opleiding bekijken</span>
					<small>Bekijk de opleiding van <?php the_title(); ?></small>
					<i class="fa fa-angle-right"></i>
				</a>					
			</aside>										
								
			<hr class="col-xs-12 no-padding" />
		</div>			
		
			
	<!-- End Col-md-8 -->
	</div>
</section>

<!-- Secundary Information -->
<section class="secundary">
	<div class="container">
		<span class="heading">Meer opleidingen bij Magistrum:</span>
		<div class="col-xs-12 no-padding-xs col-sm-12 col-md-8 page-sec-content no-padding-left">
			<div class="edu col-xs-12">
				<ul class="educations col-xs-12 no-padding">

					<?php
					$args = array(
						'post_type' => 'opleiding',
						'posts_per_page' => 3,
						'orderby' => 'rand',
						'post__not_in' => $linked_course
					);
					$courses = new WP_Query($args);
					if($courses->have_posts()) : while($courses->have_posts()) : $courses->the_post(); ?>

						<li>
							<h3><a href="<?php the_permalink(); ?>"><i class="fa fa-graduation-cap"></i>&nbsp;<?php the_title(); ?></a></h3>

							<span class="col-xs-12 col-sm-8 col-md-10 no-padding-left intro">
								<p><?php the_field('opleiding_intro'); ?></p>
							</span>

							<a class="col-xs-12 col-sm-4" href="<?php the_permalink(); ?>">
								<span class="hidden-lg">Bekijk deze opleiding</span>
								<i class="fa fa-angle-right"></i>
							</a>
						</li>

					<li>
					<?php endwhile; endif; ?>
				</ul>
				<!-- / End Education List -->
			</div>
			<!-- / End .edu -->
		</div>


	</div>
</section>

<?php get_footer(); ?>