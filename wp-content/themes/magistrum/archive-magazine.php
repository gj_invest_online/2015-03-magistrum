<?php get_header(); ?>

<!-- Start Page Content Section -->
	<section class="page magazine-overview grey-bg row-fluid">
		<div class="container">
			
			<h1 class="pagetitle col-xs-12 no-padding">Magazine</h1>

			<!-- Start col-md-8 -->
			<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-left no-padding-xs margin-bottom-30">
				
				<?php $counter = 0; ?>
				
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
					<?php if ($counter != 3) : ?>
						<article class="col-xs-12 no-padding margin-bottom-40 magazine-item">
							<figure>
								<?php the_post_thumbnail(); ?>
							</figure>
							<h3>
								<?php the_title(); ?>
								<small><?php the_time('j F Y, H:i'); ?></small>
								<i class="fa fa-angle-right"></i>
							</h3>
	
							<?php if ( has_post_format( 'video' )) :
								the_content();
							else : ?>
	
							<div>
								<?php the_excerpt(); ?>
								<a class="more-link" href="<?php the_permalink(); ?>">Lees verder</a>
							</div>
	
							<?php endif; ?>
	
							<footer>
								<div class="pull-left">
									<i class="fa fa-tag"></i> <?php the_tags(); ?>
								</div>
								<div class="pull-right">
									 <?php echo wpfai_social(); ?>
								</div>
							</footer>
						</article>
						
						<?php $counter ++; ?>
					<?php else : ?>	
						<section class="news-earlier">
					    
							<article class="col-xs-12 no-padding margin-bottom-40 magazine-item">
								<a href="<?php the_permalink(); ?>">
									<figure>
										<?php the_post_thumbnail(); ?>
									</figure>			
									<h3>
										<?php the_title(); ?>
										<small><?php the_time('j F Y, H:i'); ?></small>
										<i class="fa fa-angle-right"></i>							
									</h3>
								</a>					
							</article>
					<?php endif; ?>		
								
				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, geen magazine berichten gevonden' ); ?></p>
				<?php endif; ?>


				<!-- EARLIER NEWS -->

				<hr class="col-xs-12 no-padding" />

				<!-- Pagination -->
				<?php get_template_part('templates/pagination'); ?>
				
			<!-- End Col-md-8 -->
			</div>

			<aside class="col-xs-12 col-sm-12 col-md-4 pull-right sidebar no-padding-xs no-padding-right">
				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<div class="widget widget-default widget-archive">
						<span class="widget-heading">Archief</span>
						<ul>
							<?php 
							$args = array (
								'post_type' => 'magazine',
								'format' => 'custom',
								'limit' => 1,
								'before' => '<li><i class="fa fa-caret-right"></i>&nbsp;',
								'after' => '<small class="pull-right">Archief tonen&nbsp;<i class="fa fa-angle-right"></i></small></li>'
							);
							wp_get_archives_cpt( $args ); ?>
						</ul>
					</div>
				</div>
				<!-- End Widget -->

				<?php if ( is_active_sidebar( 'magazine_news_widget' ) ) : ?>
					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<?php dynamic_sidebar( 'magazine_news_widget' ); ?>
					</div>
				<?php endif; ?>
				<!-- End Widget -->

				<?php if ( is_active_sidebar( 'image_widget' ) ) : ?>
					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<?php dynamic_sidebar( 'image_widget' ); ?>
					</div>
				<?php endif; ?>

			</aside>
			
		</div><!-- ./ End Main Container -->
	</section><!-- ./ End Section page content -->

<?php get_footer(); ?>