<?php get_header(); ?>

    <!-- Start Page Content Section -->
    <section class="page testimonial-overview row-fluid grey-bg">
        <div class="container">

            <div class="row intro-large">
                <div class="col-xs-12 col-sm-7 col-md-8 no-padding-left">
                    Wij zijn bij Magistrum bijzonder<br/> trots op onze (oud)studenten. Dit is hun verhaal!
                </div>

                <aside class="col-xs-12 col-sm-5 col-md-3 pull-right ctas">
                    <a href="<?php echo bloginfo('url'); ?>/opleidingen" class="cta cta-red">
                        <span>Opleidingen</span>
                        <small>Bekijk hier alle opleidingen</small>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </aside>

                <hr class="col-xs-12 no-padding" />
            </div>

            <div class="phone-filters col-xs-12 no-padding hidden-md hidden-lg">
                <?php $args = array(
                    'post_type' => 'testimonial',
                    'posts_per_page' => -1
                );
                ?>
                <?php $testimonials = new WP_Query($args); ?>
                <span>Alles tonen <i class="fa fa-caret-down"></i></span>
                <div class="filters">
                    <div class="filter" data-filter="all">Alles Tonen</div>
                    <?php
                    if($testimonials->have_posts()): while($testimonials->have_posts()): $testimonials->the_post();
                        $course = get_field('linked_opleiding');
                        $course_title = get_the_title($course[0]);
                        $i = 1;
                        echo '<div class="filter" data-filter=".course_' . $i . '">' . $course_title . '</div>';
                        $i++;
                    endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <!-- Second Course Loop -->
            <?php rewind_posts(); ?>

            <!-- Start col-md-8 -->
            <div class="col-xs-12 col-sm-12 col-md-9 page-content no-padding-right pull-right no-padding-xs margin-bottom-30">
                <?php $i = 1; ?>

                <?php while($testimonials->have_posts()): $testimonials->the_post(); ?>
                    <?php $course = get_field('linked_opleiding'); ?>
                    <?php $course_title = get_the_title($course[0]); ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 mix <?php echo 'course_' . $i; ?>">
                        <a href="<?php the_permalink(); ?>" class="item testimonial col-xs-12 no-padding">
                            <div class="mask">
                                <?php the_post_thumbnail(); ?>
                            </div>
                                <span>
                                    <span class="name"><?php the_title(); ?></span>
                                    <span class="study"><?php echo $course_title; ?></span>
                                    <blockquote class="hidden-xs hidden-sm"><?php echo excerpt_limit(8, '..'); ?></blockquote>
                                    <hr class="col-xs-12 visible-xs no-padding" />
                                    <div class="more-link">
                                        Lees het verhaal van <?php the_title(); ?> <i class="fa fa-angle-right"></i>
                                    </div>
                                </span>
                        </a>
                    </div>
                    <?php $i++; ?>
                <?php endwhile; ?>

                <!-- End Col-md-8 -->
            </div>

            <!-- Third Course Loop -->
            <?php rewind_posts(); ?>
            <aside class="col-xs-12 col-sm-12 col-md-3 sidebar no-padding-xs no-padding-right hidden-sm hidden-xs">

                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                    <div class="widget widget-default widget-filters">
                        <span class="widget-heading">Filter op interesse:</span>
                        <div class="filter" data-filter="all">Alles Tonen</div>

                        <?php
                        $i = 1;
                        while($testimonials->have_posts()): $testimonials->the_post();
                            $course = get_field('linked_opleiding');
                            $course_title = get_the_title($course[0]);

                            echo '<div class="filter" data-filter=".course_' . $i . '">' . $course_title . '</div>';
                            $i++;
                        endwhile;
                        ?>
                    </div>
                    <!-- End widget -->
                </div>

            </aside>

            <!-- ./ End Main Container -->
        </div>
        <!-- ./ End Section page content -->
    </section>

<?php get_footer(); ?>