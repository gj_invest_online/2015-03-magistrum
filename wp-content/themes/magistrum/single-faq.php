<?php get_header(); ?>

    <!-- Start Page Content Section -->
    <section class="page news-events-detail row-fluid">
        <div class="container">


            <!-- Start col-md-8 -->
            <div class="col-xs-12 col-sm-12 col-md-8 page-content news-content no-padding-right no-padding-xs margin-bottom-30">
            	
                <?php while(have_posts()) : the_post();
					// Get the faq category
					
                    $terms = get_the_terms( get_the_ID(), 'onderwerp' );
					// If the faq has a category assign it
                    if($terms) {
                        foreach($terms as $term) {
                            $term_id = $term->term_id;
                            $category = ucfirst($term->name) . ' (' . $cat_count = $term->count .')';
                        }
                    } else {
                        $category = 'Veelgestelde vraag';
                    }
                ?>

                    <h1 class="pagetitle col-xs-12 col-lg-12 no-padding"><?php the_title(); ?></h1>

                    <div class="meta meta-news col-xs-12 no-padding">
                        Categorie: <span><?php echo $category; ?></span>
                    </div>
                    <?php the_content(); ?>
                <?php endwhile; ?>


                <hr class="col-xs-12 no-padding" />


                <h3>Alle vragen in deze categorie</h3>
                <!-- Start panel group -->
                <div class="panel-group faq-categories" id="questions" role="tablist" aria-multiselectable="true">

                    <!-- Start Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title active">
                                <a data-toggle="collapse" data-parent="#questions" href="#1" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                                    <?php echo $category; ?>
                                    <small>Verberg vragen</small>
                                </a>
                            </h4>
                        </div>
                        <div id="1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" style="height: 0px;">
                            <div class="panel-body">
                                <ul class="col-xs-12 no-padding">
                                    <?php
                                        $questions = get_posts(array (
                                        'showposts' => -1,
                                        'post_type' => 'faq',
                                        'tax_query' => array (
                                            array (
                                                'taxonomy' => 'onderwerp',
                                                'field' => 'term_id',
                                                'terms' => $term_id
                                            ),
                                        'orderby' => 'title',
                                        'order' => 'ASC')
                                        ));
                                        if(!empty($questions)) :
                                            foreach($questions as $question) : ?>
                                                <li>
                                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?> <i class="fa fa-angle-right"></i></a>
                                                    <?php the_content(); ?>
                                                </li>
                                            <?php endforeach;
                                        endif;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End panel -->
                </div>

                <!-- End Col-md-8 -->
            </div>

            <aside class="col-xs-12 col-sm-12 col-md-3 pull-right sidebar no-padding-xs">

                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                    <?php if ( is_active_sidebar( 'course_widget_links' ) ) : ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                            <?php dynamic_sidebar( 'course_widget_links' ); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                    <?php if ( is_active_sidebar( 'magazine_news_widget' ) ) : ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                            <?php dynamic_sidebar( 'magazine_news_widget' ); ?>
                        </div>
                    <?php endif; ?>
                    <!-- End Widget -->
                </div>

            </aside>

            <!-- ./ End Main Container -->
        </div>
        <!-- ./ End Section page content -->
    </section>

<?php get_footer(); ?>