<?php
/**
 * Modified Recent_Posts widget class to add my own markup
 *
 */
class IO_Widget_FAQ extends WP_Widget {

    function __construct() {
        parent::__construct(
            'widget_io_faq', // Base ID
            __( 'FAQ Lijst' ), // Name
            array( 'description' => __( 'Selectie van FAQs' ), ) // Args
        );
    }

    public function widget($args, $instance) {

        $faq_title = ( ! empty( $instance['faq_title'] ) ) ? $instance['faq_title'] : __( 'Veelgestelde vragen' );

        /** This filter is documented in wp-includes/default-widgets.php */
        $faq_title = apply_filters( 'widget_title', $faq_title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;

        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query( apply_filters( 'widget_posts_args', array(
            'post_type'           => 'faq',
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ) ) );

        if ($r->have_posts()) : ?>
            <?php echo $args['before_widget']; ?>
            <?php if ( $faq_title ) {
                echo $args['before_title'] . $faq_title . $args['after_title'];
            }
            while ( $r->have_posts() ) : $r->the_post(); ?>
                <?php
                $post_type = get_post_type();
                if ( $post_type ) :
                    $post_type_data = get_post_type_object( $post_type );
                    $post_type_slug = $post_type_data->rewrite['slug'];
                endif;

                $terms = get_the_terms( get_the_ID(), 'onderwerp' );
                // If the faq has a category assign it
                if($terms) {
                foreach($terms as $term) {
                    $category = ucfirst($term->name);
                }
                } else {
                    $category = 'Veelgestelde vraag';
                }
                ?>
                <article>
                    <h4><?php echo $category; ?></h4>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?><i class="fa fa-angle-right"></i></a>
                </article>
            <?php endwhile; ?>

            <a class="more-link" href="<?php echo bloginfo( 'url' ); ?>/<?php echo $post_type_slug; ?>">Ik heb een andere vraag <i class="fa fa-angle-right"></i></a>

            <?php echo $args['after_widget']; ?>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['faq_title'] = strip_tags($new_instance['faq_title']);
        $instance['number'] = (int) $new_instance['number'];
        $this->flush_widget_cache();

        $alloptions = wp_cache_get( 'alloptions', 'options' );
        if ( isset($alloptions['widget_io_recent_entries']) )
            delete_option('widget_io_recent_entries');

        return $instance;
    }

    public function flush_widget_cache() {
        wp_cache_delete('widget_io_recent_posts', 'widget');
    }

    public function form( $instance ) {
        $faq_title     = isset( $instance['faq_title'] ) ? esc_attr( $instance['faq_title'] ) : '';
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'faq_title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'faq_title' ); ?>" name="<?php echo $this->get_field_name( 'faq_title' ); ?>" type="text" value="<?php echo $faq_title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
    <?php
    }
}

function io_faq_widget_init() {
    register_widget('IO_Widget_FAQ');
}
add_action('widgets_init', 'io_faq_widget_init');
?>