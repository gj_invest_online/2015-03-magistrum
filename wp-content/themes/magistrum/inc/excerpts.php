<?php
if (!function_exists('get_excerpt_limit')) {
    function get_excerpt_limit($limit, $readmore = '[...]') {
        $text = strip_shortcodes( get_the_content() );
        $text = wpautop( $text );
        if ($readmore !== false) {
            $excerpt_more = apply_filters('excerpt_more', $readmore);
        } else {
            $excerpt_more = ' &hellip;';
            remove_filter('get_the_excerpt', 'force_readmore_excerpt');
        }

        $text = wp_trim_words($text, $limit, '');

        $char_limit = ( $limit * 9 );	// 9 being upperbound word limit
        if ( strlen($text) > $char_limit) {
            $text = substr($text, 0, strrpos(substr($text, 0, $char_limit), ' '));
        }

        $text .= $excerpt_more;
        $text = apply_filters('get_the_excerpt', $text);

        return $text;
    }
}

if (!function_exists('excerpt_limit')) {
    function excerpt_limit($limit, $readmore = '[...]') {
        $text = get_excerpt_limit($limit, $readmore);
        return wpautop($text);
    }
}

if (!function_exists('get_styled_excerpt')) {
    function get_styled_excerpt(){
        $text = strip_shortcodes( get_the_content() );
        $text = wpautop( $text );
        $excerpt_length = apply_filters('excerpt_length', 50);
        $excerpt_more = apply_filters('excerpt_more', ' [...]');

        $words_array = preg_split( "/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY );
        $sep = ' ';
        if ( count( $words_array ) > $excerpt_length ) {
            array_pop( $words_array );
            $text = implode( $sep, $words_array );
            $text = force_balance_tags( $text );
            $text = $text . $excerpt_more;
        } else {
            $text = implode( $sep, $words_array );
            $text = force_balance_tags( $text );
        }

        return apply_filters('styled_excerpt', $text);
    }
}

if (!function_exists('the_styled_excerpt')) {
    function the_styled_excerpt() {
        echo get_styled_excerpt();
    }
}

if (!function_exists('force_readmore_excerpt')) {
    function force_readmore_excerpt($output) {
        return $output . ' ' . '<a href="' . get_permalink() . '" class="more">' . __('Lees meer') .  '</a>';
    }
}
?>