<?php

function io_scripts() {

    wp_enqueue_style( 'hbh_style', get_stylesheet_uri() );
    //wp_enqueue_style( 'io_theme_style', get_template_directory_uri() . '/assets/css/io_styles.min.css' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap/css/bootstrap.css' );
	wp_enqueue_style( 'global', get_template_directory_uri() . '/assets/css/global.css' );
	wp_enqueue_style( 'magistrum', get_template_directory_uri() . '/assets/css/magistrum.css' );
	wp_enqueue_style( 'mq-desktop', get_template_directory_uri() . '/assets/css/mq-desktop.css' );
	wp_enqueue_style( 'mq-tablets-landscape', get_template_directory_uri() . '/assets/css/mq-tablets-landscape.css' );
	wp_enqueue_style( 'mq-tablets-portrait', get_template_directory_uri() . '/assets/css/mq-tablets-portrait.css' );
	wp_enqueue_style( 'mq-mobile', get_template_directory_uri() . '/assets/css/mq-mobile.css' );

    /* Add  JS */
    //wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/public/js/bootstrap.js', array('jquery'), '', true );

}
add_action( 'wp_enqueue_scripts', 'io_scripts' );

function io_admin_scripts() {
	/* Add  CSS */
    wp_enqueue_style( 'theme-admin-style', get_stylesheet_directory_uri() . '/assets/css/theme-admin.css' );
}

add_action( 'admin_enqueue_scripts', 'io_admin_scripts' );



//dequeue css from plugins
if (!function_exists('io_remove_social_share_style')) {
	
	add_action('wp_enqueue_scripts', 'io_remove_social_share_style');
	
	function io_remove_social_share_style()  {
		global $wp_styles;
		$handle = 'wpfai_style';
		
		if (wp_style_is('wpfai_style')) {
			wp_dequeue_style( 'wpfai_style' );
			wp_deregister_style('wpfai_style');
		}
	}
}
// Dequeue CF7 Styles
if(!function_exists('io_remove_cf7_style')) {
	add_action('wp_enqueue_scripts', 'io_remove_cf7_style');
	
	function io_remove_cf7_style()  {
		global $wp_styles;
		$handle = 'contact-form-7';
		
		if (wp_style_is('contact-form-7')) {
			wp_dequeue_style( 'contact-form-7' );
			wp_deregister_style('contact-form-7');
		}
	}
}
?>