<?php

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function io__widgets_init() {

    register_sidebar( array(
        'name'          => __( 'Afbeeldingen widget' ),
        'id'            => 'image_widget',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget widget-default widget-banner">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );
	register_sidebar( array(
        'name'          => __( 'Magazine Nieuws widget' ),
        'id'            => 'magazine_news_widget',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget widget-default widget-news">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-heading">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => __( 'FAQ lijst Widget' ),
        'id'            => 'faq_list_widget',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget widget-default widget-faq">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-heading">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Contact widget' ),
        'id'            => 'io_contact_widget',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget widget-default widget-contact">',
        'after_widget'  => '</div>',
        'before_title'  => '<span class="widget-heading">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Opleidingen links' ),
        'id'            => 'course_widget_links',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="ctas">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Kolom 1' ),
        'id'            => 'footer_column_1',
        'description'   => 'Eerste kolom in de footer',
        'before_widget' => '<div class="col-xs-12 col-sm-4 col-md-3 no-padding-left">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Kolom 2' ),
        'id'            => 'footer_column_2',
        'description'   => 'Tweede kolom in de footer',
        'before_widget' => '<div class="col-xs-12 col-sm-4 col-md-3 no-padding-left">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Kolom 3' ),
        'id'            => 'footer_column_3',
        'description'   => 'Derde kolom in de footer',
        'before_widget' => '<div class="col-xs-12 col-sm-4 col-md-3 no-padding-left">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer Kolom 4' ),
        'id'            => 'footer_column_4',
        'description'   => 'Vierde kolom in de footer',
        'before_widget' => '<div class="col-xs-12 col-sm-12 col-md-3 no-padding-right">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
}



add_action( 'widgets_init', 'io__widgets_init' );

?>