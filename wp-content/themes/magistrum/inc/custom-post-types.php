<?php

// **************************************************************************************************
// Opledingen
// **************************************************************************************************

$courses = new Super_Custom_Post_Type('opleiding', 'Opleiding', 'Opleidingen', array(
	'rewrite' => array( 'slug' => 'opleidingen' ),
));
$courses->set_icon( 'graduation-cap' );

$courses->add_meta_box( array(
    'id' => 'contactgegevens_opleiding',
    'context' => 'side',
    'priority' => 'core',
    'fields' => array(
        'course_contact_person' => array( 'label' => 'Contactpersoon', 'type' => 'text', ),
        'course_contact_email' => array( 'label' => 'Email', 'type' => 'enail' ),
        'course_contact_phone' => array( 'label' => 'Telefoon', 'type' => 'text' ),
    )
) );

// **************************************************************************************************
// Locaties
// **************************************************************************************************

$locations = new Super_Custom_Post_Type('locatie', 'Locatie', 'Locaties');
$locations->set_icon( 'map-marker' );

// **************************************************************************************************
// Testimonials
// **************************************************************************************************

$testimonials = new Super_Custom_Post_Type( 'testimonial' );
$testimonials->set_icon( 'heart' );

// **************************************************************************************************
// Magazine
// **************************************************************************************************

$magazine = new Super_Custom_Post_Type( 'magazine', 'Magazine', 'Magazines', array(
	'taxonomies' 	=> array('category', 'post_tag')
) );
$magazine->set_icon( 'file-image-o' );

// **************************************************************************************************
// Events
// **************************************************************************************************

$events = new Super_Custom_Post_Type( 'event' );
$events->set_icon( 'time' );


// **************************************************************************************************
// FAQ
// **************************************************************************************************

$faq = new Super_Custom_Post_Type( 'faq', 'FAQ', 'FAQ', array(
    'public' => true,
    'rewrite' => array('slug' => 'veelgestelde-vragen')
) );
$faq->set_icon( 'question' );

$faq_subjects = new Super_Custom_Taxonomy( 'onderwerp', 'Onderwerp', 'Onderwerpen', 'category' );
connect_types_and_taxes( $faq, array( $faq_subjects ) );

// **************************************************************************************************
// CUSTOM META PAGE MENU SELECT
// **************************************************************************************************
$menu_objects = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
$menu_array = array();
foreach($menu_objects as $menu_object) {
	$item = $menu_object->name;
	$menu_array[] = $item;
}


$page_meta = new Super_Custom_Post_Meta( 'page' );
$page_meta->add_meta_box( array(
    'id' => 'selected_menu',
    'context' => 'side',
    'fields' => array(
        '_menus' => array( 
	        'label' => 'Selecteer menu', 
	        'type' => 'select', 
	        'options' => $menu_array,
	        'default' => $menu_array[0]
		)
    )
));

?>