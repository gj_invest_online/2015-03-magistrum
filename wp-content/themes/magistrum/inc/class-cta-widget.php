<?php
/**
 * Adds Foo_Widget widget.
 */
class CTA_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'cta_widget', // Base ID
            __( 'Call to Action' ), // Name
            array( 'description' => __( 'Plaats een call to action button' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        $link = get_the_permalink($instance['cta_link_page']);

        echo $args['before_widget']; ?>
        <a href="<?php echo $link; ?>" class="cta cta-<?php echo $instance['cta_bg_color']; ?>">
            <span><?php echo $instance['cta_title']; ?></span>
            <small><?php echo $instance['cta_subtitle']; ?></small>
            <i class="fa fa-angle-right"></i>
        </a>
        <?php echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $cta_title = ! empty( $instance['cta_title'] ) ? $instance['cta_title'] : __( 'CTA' );
        $cta_subtitle = ! empty( $instance['cta_subtitle'] ) ? $instance['cta_subtitle'] : __( 'Subtitel' );
        $cta_bg_color = ! empty( $instance['cta_bg_color'] ) ? $instance['cta_bg_color'] : "red" ;
        $cta_link_page = ! empty( $instance['cta_link_page'] ) ? $instance['cta_link_page'] : "" ;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'cta_title' ); ?>"><?php _e( 'CTA:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'cta_title' ); ?>" name="<?php echo $this->get_field_name( 'cta_title' ); ?>" type="text" value="<?php echo esc_attr( $cta_title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'cta_subtitle' ); ?>"><?php _e( 'CTA Sub:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'cta_title' ); ?>" name="<?php echo $this->get_field_name( 'cta_subtitle' ); ?>" type="text" value="<?php echo esc_attr( $cta_subtitle ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'cta_bg_color' ); ?>"><?php _e( 'CTA kleur:' ); ?></label>
            <input type="radio" id="cta_bg_color_red" name="<?php echo $this->get_field_name( 'cta_bg_color' ); ?>" value="red" <?php checked( $cta_bg_color, 'red' )?>>Rood
            <input type="radio" id="cta_bg_color_blue" name="<?php echo $this->get_field_name( 'cta_bg_color' ); ?>" value="blue" <?php checked( $cta_bg_color, 'blue' )?>>Blauw
            <input type="radio" id="cta_bg_color_green" name="<?php echo $this->get_field_name( 'cta_bg_color' ); ?>" value="green" <?php checked( $cta_bg_color, 'green' )?>>Groen
        </p>
        <p>
            <label><?php _e( 'Selecteer pagina' ); ?></label>

            <select name="<?php echo $this->get_field_name( 'cta_link_page' ); ?>">

                <?php // Query for listing all pages in the select box loop
                $all_wp_pages = new WP_Query(array(
                    'post_type' => 'page',
                    'posts_per_page' => -1
                ));
                if ( $all_wp_pages->have_posts() ) :
                    while ( $all_wp_pages->have_posts() ) : $all_wp_pages->the_post(); ?>
                        <?php $id = get_the_id(); ?>
                        <option value="<?php echo $id; ?>" <?php if(isset($instance['cta_link_page'])) : selected( $instance['cta_link_page'], $id ); endif;?>><?php the_title(); ?></option>

                    <?php endwhile; ?>

                    <?php wp_reset_postdata(); ?>

                <?php endif; ?>

            </select>
        </p>
    <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['cta_title'] = ( ! empty( $new_instance['cta_title'] ) ) ? strip_tags( $new_instance['cta_title'] ) : '';
        $instance['cta_subtitle'] = ( ! empty( $new_instance['cta_title'] ) ) ? strip_tags( $new_instance['cta_subtitle'] ) : '';
        $instance['cta_bg_color'] = ( isset( $new_instance['cta_bg_color'] ) && $new_instance['cta_bg_color'] != $instance['cta_bg_color'] )  ? $new_instance['cta_bg_color'] : 'red';
        $instance['cta_link_page'] = ( isset( $new_instance['cta_link_page'] ) && $new_instance['cta_link_page'] != $instance['cta_link_page'] )  ? $new_instance['cta_link_page'] : '';

        return $instance;
    }

} // class CTA_Widget

// register Foo_Widget widget
function register_cta_widget() {
    register_widget( 'CTA_Widget' );
}
add_action( 'widgets_init', 'register_cta_widget' );
?>