<?php

register_nav_menus( array(
    'main_nav'      => 'Hoofdnavigatie',
    'sub_main_nav'  => 'Subnavigatie hoofdmenu',
    'about_menu'    => 'Over Magistrum menu',
    'footer_menu_1' => 'Footer menu kolom 1',
    'footer_menu_2' => 'Footer menu kolom 2',
    'footer_menu_3' => 'Footer menu kolom 3'
) );


// **********************************************************************************************************
// MAIN NAVIGATION
// **********************************************************************************************************
function mg_main_menu_items() {
    // Get the nav menu based on $menu_name (same as 'theme_location' or 'menu' arg to wp_nav_menu)
    // This code based on wp_nav_menu's code to get Menu ID from menu slug

    $menu_name = 'main_nav';

    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    	// What we need to set up the menu
    	global $wp;
        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
		$menu_list = '';

        foreach ( (array) $menu_items as $key => $menu_item ) {
			$current_url = $current_url = home_url(add_query_arg(array(),$wp->request));
			$url = rtrim($menu_item->url, '/');
			
            if ($url == $current_url) {
                array_push($menu_item->classes , 'active');
            }
            $title = $menu_item->title;
            $classes = implode($menu_item->classes);

            $menu_list .= '<li class="' . $classes . '">';
            $menu_list .= '<a href="' . $url . '">' . $title . ' <i class="visible-xs fa fa-angle-right"></i></a>';
            $menu_list .= '</li>';
        }

    } else {
        $menu_list = '<ul class="nav navbar-nav navbar-right navbar-main"><li>Menu "' . $menu_name . '" not defined. <i class="visible-xs fa fa-angle-right"></i></li></ul>';
    }
    echo $menu_list;
}


// **********************************************************************************************************
// SUBMENU NAVIGATION
// **********************************************************************************************************
function mg_sub_menu_items() {
    // Get the nav menu based on $menu_name (same as 'theme_location' or 'menu' arg to wp_nav_menu)
    // This code based on wp_nav_menu's code to get Menu ID from menu slug

    $sub_menu_name = 'sub_main_nav';

    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $sub_menu_name ] ) ) {
        $menu = wp_get_nav_menu_object( $locations[ $sub_menu_name ] );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
		
		$sub_menu_list = '';

        foreach ( (array) $menu_items as $key => $menu_item ) {
            $title = $menu_item->title;
            $url = $menu_item->url;
            $classes = implode($menu_item->classes);
			
            $sub_menu_list .= '<li class="' . $classes . '">';
            $sub_menu_list .= '<a href="' . $url . '">' . $title . '</a>';
            $sub_menu_list .= '</li>';
        }

    } else {
        $sub_menu_list = '<ul class="nav navbar-nav navbar-right navbar-main"><li>Menu "' . $sub_menu_name . '" not defined. <i class="visible-xs fa fa-angle-right"></i></li></ul>';
    }
    echo $sub_menu_list;
}

// **********************************************************************************************************
// ABOUT MAGISTRUM NAVIGATION
// **********************************************************************************************************

function mg_about_menu_items() {
    // Get the nav menu based on $menu_name (same as 'theme_location' or 'menu' arg to wp_nav_menu)
    // This code based on wp_nav_menu's code to get Menu ID from menu slug
    
    $page_id = get_queried_object_id();
	$meta = get_post_meta($page_id, '_menus', true);
	if( $meta != '' ) {
		$menu_name = $meta;
	} else {
		$menu_name = 'about_menu';
	}
	
	$menu_list = '<nav class="col-xs-12 no-padding">';

    if (wp_get_nav_menu_object($menu_name)) {
        // What we need to set up the menu
        global $wp;
        $menu = wp_get_nav_menu_object( $menu_name );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $count = 0;
        $submenu = false;

        foreach ( (array) $menu_items as $key => $menu_item ) {
            $current_url = $current_url = home_url(add_query_arg(array(),$wp->request));
            $url = rtrim($menu_item->url, '/');
            $title = $menu_item->title;

            if ($url == $current_url) {
                array_push($menu_item->classes , 'active');
            }

            $classes = implode($menu_item->classes);

            if( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;

                $menu_list .= '<li class="' . $classes . '">';
                $menu_list .= '<a href="' . $url . '"><i class="fa fa-caret-right"></i> ' . $title . '</a>';
            }

            if ( $parent_id == $menu_item->menu_item_parent ) {

                if ( !$submenu) {
                    $submenu = true;
                    $menu_list .= '<ul class="sub-nav">';
                }

                $menu_list .= '<li><a href="' . $url .'">' . $title . '<i class="fa fa-angle-right"></i></a></li>';

                if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ) {
                    $menu_list .= '</ul>';
                    $submenu = false;
                }
            }

            if ( isset($menu_items[ $count + 1 ]->menu_item_parent) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ){
                $menu_list .= '</li>';
                $submenu = false;
            }
            $count++;
        }
    }

	$menu_list .= '</nav>';

    if($menu_list != '') {
        echo $menu_list;
    }
}

// **********************************************************************************************************
// ABOUT MAGISTRUM MOBILE NAVIGATION
// **********************************************************************************************************
function mg_about_menu_mobile() {
    // Get the nav menu based on $menu_name (same as 'theme_location' or 'menu' arg to wp_nav_menu)
    // This code based on wp_nav_menu's code to get Menu ID from menu slug

    $menu_name = 'about_menu';

    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
        // What we need to set up the menu
        global $wp;
        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list = '';
        $count = 0;
        $submenu = false;

        foreach ( (array) $menu_items as $key => $menu_item ) {
            $current_url = $current_url = home_url(add_query_arg(array(),$wp->request));
            $url = rtrim($menu_item->url, '/');
            $title = $menu_item->title;

            if ($url == $current_url) {
                array_push($menu_item->classes , 'active');
            }

            $classes = implode($menu_item->classes);

            if( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;

                $menu_list .= '<li class="' . $classes . '">';
                $menu_list .= '<a href="' . $url . '"><i class="fa fa-caret-right"></i> ' . $title . '</a>';
            }

            if ( $parent_id == $menu_item->menu_item_parent ) {

                if ( !$submenu) {
                    $submenu = true;
                    $menu_list .= '<ul class="sub-nav">';
                }

                $menu_list .= '<li><a href="' . $url .'">' . $title . '<i class="fa fa-angle-right"></i></a></li>';

                if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ) {
                    $menu_list .= '</ul>';
                    $submenu = false;
                }
            }

            if ( isset($menu_items[ $count + 1 ]->menu_item_parent) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ){
                $menu_list .= '</li>';
                $submenu = false;
            }
            $count++;
        }
    }

    if($menu_list != '') {
        echo $menu_list;
    }
}

// **********************************************************************************************************
// FOOTER NAVIGATION
// **********************************************************************************************************
add_filter( 'wp_nav_menu_args', function( array $args ) {
    if (
        'footer_menu_1' == $args['menu'] ||
        'footer_menu_2' == $args['menu'] ||
        'footer_menu_3' == $args['menu']
    ) {
        return array_merge($args, array(
            'link_before' => '<i class="fa fa-angle-right"></i>'
        ));
    }
    return $args;
} );


?>