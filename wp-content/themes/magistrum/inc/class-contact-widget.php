<?php
/**
 * Adds The contact widget with the call-me contact form
 */
class IO_Contact_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'io_contact_widget', // Base ID
            __( 'Contactgegevens Magistrum' ), // Name
            array( 'description' => __( 'Geef hier de contactgegevens op' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        extract( $args );
        $io_show_form = $instance['io_show_form'] ? 'true' : 'false';
        echo $args['before_widget'];
        echo '<div class="col-md-12 col-sm-6 col-xs-12 no-padding">';

        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        } ?>

        <span class="location" xmlns="http://www.w3.org/1999/html">
            <strong><i class="fa fa-map-marker"></i> <?php echo $instance['name'] . ', ' . $instance['phone_num']; ?></strong>
            <?php echo $instance['address'] . '<br>'; ?>
            <?php echo $instance['zip'] . ', ' . $instance['city']; ?>
        </span>
        </div>
        <?php
        /**
	     * The Contact Form
	     */
        if($instance['io_show_form'] == 'on') : ?>
            <hr class="col-xs-12 no-padding hidden-sm">
            <div class="col-md-12 col-sm-6 col-xs-12 no-padding">
            	<span class="widget-heading"><?php echo get_the_title(213); ?></span>
                <?php echo do_shortcode( '[contact-form-7 id="213" title="Wij bellen u"]' ); ?>
            </div>
        <?php endif; ?>



        <?php echo $args['after_widget']; ?>
    <?php }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Contact' );
        $name = ! empty( $instance['name'] ) ? $instance['name'] : __( 'Magistrum' );
        $address = ! empty( $instance['address'] ) ? $instance['address'] : __( 'Heerbaan 14-40' );
        $zip = ! empty( $instance['zip'] ) ? $instance['zip'] : __( '4817 NL' );
        $city = ! empty( $instance['city'] ) ? $instance['city'] : __( 'Breda' );
        $phone_num = ! empty( $instance['phone_num'] ) ? $instance['phone_num'] : __( '076 - 525 88 96' );
        $io_show_form = ! empty( $instance['io_show_form'] ) ? $instance['io_show_form'] : __( 'on' );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Titel:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'name' ); ?>"><?php _e( 'Naam Instelling:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Adres:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'zip' ); ?>"><?php _e( 'Postcode:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'zip' ); ?>" name="<?php echo $this->get_field_name( 'zip' ); ?>" type="text" value="<?php echo esc_attr( $zip ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'city' ); ?>"><?php _e( 'Plaats:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'city' ); ?>" name="<?php echo $this->get_field_name( 'city' ); ?>" type="text" value="<?php echo esc_attr( $city ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'phone_num' ); ?>"><?php _e( 'Telefoon:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'phone_num' ); ?>" name="<?php echo $this->get_field_name( 'phone_num' ); ?>" type="text" value="<?php echo esc_attr( $phone_num ); ?>">
        </p>
        <?php
        /**
	     * The Contact Form
	     * @Todo Select the right contact form from a dropdown
	     */
        ?>
        <p>
            <input class="checkbox" type="checkbox" value="<?php echo esc_attr( $io_show_form ); ?>" <?php if(isset($instance['io_show_form'])) : checked($instance['io_show_form'], 'on'); endif; ?> id="<?php echo $this->get_field_id('io_show_form'); ?>" name="<?php echo $this->get_field_name('io_show_form'); ?>" />
            <label for="<?php echo $this->get_field_id( 'io_show_form' ); ?>"><?php _e('Wij bellen u formulier tonen?'); ?></label>
        </p>
    <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['name'] = ( ! empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
        $instance['phone_num'] = ( ! empty( $new_instance['phone_num'] ) ) ? strip_tags( $new_instance['phone_num'] ) : '';
        $instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
        $instance['zip'] = ( ! empty( $new_instance['zip'] ) ) ? strip_tags( $new_instance['zip'] ) : '';
        $instance['city'] = ( ! empty( $new_instance['city'] ) ) ? strip_tags( $new_instance['city'] ) : '';
        $instance['io_show_form'] = $new_instance['io_show_form'];
        return $instance;
    }

} // class IO_Contact_Widget

// register IO_Contact_Widget widget
function register_io_contact_widget() {
    register_widget( 'IO_Contact_Widget' );
}
add_action( 'widgets_init', 'register_io_contact_widget' );
?>