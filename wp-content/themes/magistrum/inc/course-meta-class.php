<?php

/**
 * Calls the class on the post edit screen.
 */
function call_ioMetaClass() {
    new ioMetaClass();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_ioMetaClass' );
    add_action( 'load-post-new.php', 'call_ioMetaClass' );
}

/** 
 * The Class.
 */
class ioMetaClass {

	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
	    $post_types = array('opleiding');     //limit meta box to certain post types
	    if ( in_array( $post_type, $post_types )) {
			add_meta_box(
				'course_contact_details',
				__( 'Contactgegevens' ),
				array( $this, 'render_meta_box_content' ),
				$post_type,
				'side',
				'default'
			);
    	}
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
	
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['io_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['io_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'io_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'opleiding' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */

		// Sanitize the user input.
		$mydata_person = sanitize_text_field( $_POST['course_contact_person'] );
		$mydata_email = sanitize_email( $_POST['course_contact_email'] );
		$mydata_phone = sanitize_text_field( $_POST['course_contact_phone'] );
		// Update the meta field.
		update_post_meta( $post_id, 'course_contact_person', $mydata_person );
		update_post_meta( $post_id, 'course_contact_email', $mydata_email );
		update_post_meta( $post_id, 'course_contact_phone', $mydata_phone );
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post, $metaboxes ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'io_inner_custom_box', 'io_inner_custom_box_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$values = get_post_custom( $post->ID );
		$contact = isset( $values['course_contact_person'] ) ? esc_attr( $values['course_contact_person'][0] ) : '';
		$email = isset( $values['course_contact_email'] ) ? esc_attr( $values['course_contact_email'][0] ) : '';
		$phone = isset( $values['course_contact_phone'] ) ? esc_attr( $values['course_contact_phone'][0] ) : '';
		
		// Display the form, using the current value.
		echo '<p>';
		echo '<label class="label-block" for="course_contact_person">Contactpersoon</label>';
		echo '<input type="text" id="course_contact_person" name="course_contact_person" value="' . esc_attr( $contact ) . '" size="25" />';
		echo '</p>';
		echo '<p>';
		echo '<label class="label-block" for="course_contact_email">Email</label> ';
		echo '<input type="email" id="course_contact_email" name="course_contact_email" value="' . esc_attr( $email ) . '" size="25" />';
		echo '</p>';
		echo '<p>';
		echo '<label class="label-block" for="course_contact_phone">Telefoon</label> ';
		echo '<input type="text" id="course_contact_phone" name="course_contact_phone" value="' . esc_attr( $phone ) . '" size="12" />';
		echo '</p>';
	}
}

?>