<?php get_header(); ?>

	<!-- Start Page Content Section -->
	<section class="page news-events-detail row-fluid">	
		<div class="container">
		
			<!-- Start col-md-8 -->
			<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-right no-padding-xs margin-bottom-30">
				
				<?php while ( have_posts() ) : the_post(); ?>
				
				<?php
				// Date and Time
				$unixtimestamp = strtotime(get_field('io_event_date'));
				$month = date_i18n('F', $unixtimestamp);
				$day = date_i18n('d', $unixtimestamp);
				
				// The Location
				$location_obj = get_field('event_location');
				$location = $location_obj[0]->post_title;
				$phone_num = get_field('locatie_telefoonnummer', $location_obj[0]->ID);
				$address = get_field('locatie_straatnaam', $location_obj[0]->ID);
				$address_num = get_field('locatie_huisnummer', $location_obj[0]->ID);
				$zip = get_field('locatie_postcode', $location_obj[0]->ID);
				
				?>
				
				<span class="date cta-grey col-xs-3 col-sm-2 col-lg-2 col-md-2 visible-xs">
					<?php echo $day; ?>
					<small><?php echo $month; ?></small>
				</span>
				
				<h1 class="pagetitle col-xs-9 col-lg-12 no-padding"><?php the_title(); ?></h1>
				
				<div class="meta col-xs-12 no-padding">
					<span class="date cta-grey col-xs-3 col-sm-2 col-lg-2 col-md-2 hidden-xs">
						<?php echo $day; ?> <small><?php echo $month; ?></small>
					</span>	
											
					<figure class="col-md-4 col-lg-4 visible-lg no-padding">
						<?php the_post_thumbnail(); ?>					
					</figure>
					
					<div class="data col-xs-12 col-sm-10 col-md-10 col-lg-6">
						<span class="col-xs-12 no-padding"><strong>Tijden:</strong> <?php the_field('io_event_start'); ?> – <?php the_field('io_event_end'); ?></span>
						<hr class="col-xs-12 no-padding" />
						<span class="location">
							<strong><i class="fa fa-map-marker"></i>&nbsp;<?php echo $location; ?>, <?php echo $phone_num; ?></strong>
							<?php echo $address; ?> <?php echo $address_num; ?>, <?php echo $zip; ?>, <a href="">Routebeschrijving</a>
						</span>
					</div>
				</div>
				<?php the_content(); ?>
				
				<?php endwhile; ?>

				<iframe style="height:383px;width:100%" src="http://magistrum.force.com/Eventformulier"></iframe>

				<!-- End Col-md-8 -->
				</div>
				
				<aside class="col-xs-12 col-sm-12 col-md-3 pull-right sidebar no-padding-xs">
					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<div class="ctas">						
							<a href="" class="cta cta-red">
								<span>Inschrijven!</span>
								<small>Schrijf je in voor de opleiding.</small>
								<i class="fa fa-angle-right"></i>
							</a>
							
							<a href="" class="cta cta-green">
								<span>Proefles</span>
								<small>Vraag hier uw gratis proefles aan.</small>
								<i class="fa fa-angle-right"></i>
							</a>
							
							<a href="" class="cta cta-blue">
								<span>Brochure</span>
								<small>Download de Avans brochure.</small>
								<i class="fa fa-angle-right"></i>
							</a>										
						</div>			
					</div>
										
					
					<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
						<div class="testimonial">
							<div class="mask">
								<img src="assets/images/placeholder/mona-wells-placeholder.jpg" alt="Mona Wells" />
							</div>
							<span>
								<span class="name">Mona Wells</span>
								<span class="study">Human Resource Management</span>
								<blockquote>
									Een uitstekende keuze! Boeiend gebracht door docenten met bovenmaatse ervaring binnen de branche.
								</blockquote>
								<hr class="col-xs-12 visible-xs no-padding" />	
								<a href="#" class="more-link">
									Lees de reis van Mona Wells <i class="fa fa-angle-right"></i>
									<hr/>
								</a>
							</span>
						</div>	
					</div>			
				</aside>
								
			<!-- ./ End Main Container -->	
			</div>
		<!-- ./ End Section page content -->		
			</div>
		</div>
	</section>



		
<?php get_footer(); ?>