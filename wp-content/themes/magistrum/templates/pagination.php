<?php
global $wp_query;

$total_pages = $wp_query->max_num_pages;

if ($total_pages > 1){

    $current_page = max(1, get_query_var('paged'));

    echo '<nav>';
    echo '<ul class="pagination">';

    $links = paginate_links(array(
        'type' => 'array',
        'prev_next' => FALSE,
    ));
    $prev_link = get_previous_posts_link('<i class="fa fa-angle-left"></i>');
    $next_link = get_next_posts_link('<i class="fa fa-angle-right"></i>');

    if($next_link) {
        $next_link = $next_link;
    } else {
        $next_link = '<a aria-label="Next" href="#"><i class="fa fa-angle-right"></i></a>';
    }

    if($prev_link) {
        $prev_link = $prev_link;
    } else {
        $prev_link = '<a aria-label="Previous" href="#"><i class="fa fa-angle-left"></i></a>';
    }

    array_unshift($links, $prev_link);
    $links[] = $next_link;

    foreach($links as $link) {
        echo '<li>' . $link;
    }

    echo '<ul>';
    echo '</nav>';

}
?>