<section class="search">

    <div class="searchfield col-xs-12 col-sm-12 col-md-12 no-padding">

        <form action="<?php echo home_url( '/' ); ?>" method="get">
            <input type="text" class="search-field" placeholder="<?php echo esc_attr_x( 'Zoeken in Magistrum...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Zoeken in Magistrum...', 'label' ) ?>" />
            <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
        </form>

    </div>

</section>