<h3>Neem contact met mij op</h3>
<div class="clearfix"></div>
<form class="form" action="" method="post" id="" name="neem-contact-met-mij-op" enctype="application/x-www-form-urlencoded">
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Aanhef<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <label class="radio"><input type="radio" id="aanhef1" name="aanhef" class="required" value="Dhr." />Dhr.</label>
            <label class="radio"><input type="radio" id="aanhef2" name="aanhef" class="required" value="Mevr." />Mevr.</label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Voornaam<span class="ef-req"> *</span></label>
        <input class="col-xs-12 col-sm-8 col-md-9 required" type="text" name="voornaam" placeholder="Type hier je voornaam" value="">
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Tussenvoegsel<span class="ef-req"></span></label>
        <input class="col-xs-12 col-sm-8 col-md-9 " type="text" name="tussenvoegsel" placeholder="Type hier je tussenvoegsel" value="">
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Achternaam<span class="ef-req"> *</span></label>
        <input class="col-xs-12 col-sm-8 col-md-9 required" type="text" name="achternaam" placeholder="Type hier je achternaam" value="">
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">E-mailadres<span class="ef-req"> *</span></label>
        <input class="col-xs-12 col-sm-8 col-md-9 required" type="text" name="e-mailadres" placeholder="Type hier je e-mailadres" value="">
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Telefoonnummer<span class="ef-req"> *</span></label>
        <input class="col-xs-12 col-sm-8 col-md-9 required" type="text" name="telefoonnummer" placeholder="Type hier je telefoonnummer" value="">
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Je vraag<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <textarea name="vraag" placeholder="Type hier je vraag"></textarea>
        </div>
    </div>
    <div class="ef-buttons">
        <div class="col-md-offset-3 col-sm-offset-4">
            <input type="submit" id="submit_button" name="submit" value="Verzenden" /></div>
    </div>
</form>