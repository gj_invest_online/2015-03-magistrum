<?php
/*
 * Borchure aanvragen
 */
?>
<h3>Brochure aanvragen</h3>
<div class="clearfix"></div>
<form class="form" role="form" data-toggle="validator">
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Aanhef<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <label class="radio"><input type="radio" id="aanhef1" name="aanhef" required value="Dhr." />Dhr.</label>
            <label class="radio"><input type="radio" id="aanhef2" name="aanhef" required value="Mevr." />Mevr.</label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Voornaam<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class=" required form-control" type="text" name="naam" placeholder="Type hier je voornaam" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Tussenvoegsel<span class="ef-req"></span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="form-control" type="text" name="tussenvoegsel" placeholder="Type hier je tussenvoegsel">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Achternaam<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="achternaam" placeholder="Type hier je achternaam" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">E-mailadres<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="email" name="e-mailadres" placeholder="Type hier je e-mailadres" data-error="Geen geldig e-mailadres" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Telefoonnummer<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="telefoonnummer" placeholder="Type hier je telefoonnummer" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Adres<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="adres" placeholder="Type hier je adres" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Postcode<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="postcode" placeholder="Type hier je postcode" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Woonplaats<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="woonplaats" placeholder="Type hier je woonplaats" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Versturen via:</label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <label class="checkbox"><input type="checkbox" name="checkbox-inline" checked=""><i></i>Email</label>
            <label class="checkbox"><input type="checkbox" name="checkbox-inline"><i></i>Post</label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3"></label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <input type="checkbox" checked="" value="Yes"/> Ja, hou me op de hoogte van het laatste nieuws van Magistrum
        </div>
    </div>
    <div class="form-group ef-buttons">
        <div class="col-md-offset-3 col-sm-offset-4">
            <input type="submit" id="submit_button" name="submit" value="Verzenden" />
        </div>
    </div>
</form>
