<?php
/*
 * Aanmeldingsformulier
 */
?>

<h3>Aanmelden opleiding</h3>
<div class="clearfix"></div>

<form class="form" role="form" data-toggle="validator">

    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Kruis aan welke opleiding van toepassing is</label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Directeur Brede School Integraal Kindcentrum</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Directeur Primair Onderwijs, Basisbekwaam</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Directeur Primair Onderwijs, Vakbekwaam</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox""><i></i>Directeur van Buiten</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Master Educational Leadership (MEL)</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Master Leadership in Education (MLE)</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Master of Business Administration (MBA)</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Middenmanagement</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Oriëntatie op leiderschap</label>
            </div>
            <div class="clearfix">
                <label class="checkbox"><input type="checkbox"><i></i>Upgrade Middenmanagement tot Basisbekwaam</label>
            </div>
        </div>
    </div>
    <hr class="col-xs-12 no-padding hidden-sm">
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Gewenste locatie<span class="ef-req"> *</span></label>
        <select class="col-xs-12 col-sm-8 col-md-9" class="form-control">
            <option>Selecteer locatie</option>
            <option>Avans Hogeschool</option>
            <option>CHS Windesheim</option>
            <option>Hogeschool Edith Stein</option>
            <option>Hogeschool IPABO</option>
            <option>Hogeschool Utrecht</option>
            <option>Katholieke PABO Zwolle</option>
            <option>Magistrum</option>
            <option>Fontys Hogeschool Kind en Educatie</option>
        </select>
    </div>

    <hr class="col-xs-12 no-padding hidden-sm">

    <div class="form-group">
        <h3>Persoonlijke gegevens</h3>
        <label class="col-xs-12 col-sm-4 col-md-3">Aanhef<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <label class="radio"><input type="radio" id="aanhef1" name="aanhef" required value="Dhr." />Dhr.</label>
            <label class="radio"><input type="radio" id="aanhef2" name="aanhef" required value="Mevr." />Mevr.</label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Voornaam<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class=" required form-control" type="text" name="naam" placeholder="Type hier je voornaam" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Tussenvoegsel<span class="ef-req"></span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="form-control" type="text" name="tussenvoegsel" placeholder="Type hier je tussenvoegsel">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Achternaam (Meisjesnaam)<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="achternaam" placeholder="Type hier je achternaam" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <hr class="col-xs-12 no-padding hidden-sm">

    <div class="form-group">
        <h3>Contactgegevens</h3>
        <label class="col-xs-12 col-sm-4 col-md-3">Straat + huisnummer<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="adres" placeholder="Type hier je adres" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Postcode + woonplaats<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="postcode" placeholder="Type hier je postcode en woonplaats" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Telefoonnummer<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="telefoonnummer" placeholder="Type hier je telefoonnummer" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">E-mailadres<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="email" name="e-mailadres" placeholder="Type hier je e-mailadres" data-error="Geen geldig e-mailadres" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <hr class="col-xs-12 no-padding hidden-sm">

    <div class="form-group">
        <h3>Bevoegd gezag</h3>
        <label class="col-xs-12 col-sm-4 col-md-3">Naam school<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="naam_school" placeholder="Naam school" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Functienaam<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="functienaam" placeholder="Functienaam" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Bevoegd gezag<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="bevoegd_gezag" placeholder="Bevoegd gezag" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Straat + Huisnummer<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="gezag_adres" placeholder="Straat + huisnummer" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Postcode + woonplaats<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="gezag_postcode_woonplaats" placeholder="postcode + woonplaats" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">Telefoonnummer<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="text" name="gezag_telefoonnummer" placeholder="Telefoonnummer" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3">E-mailadres<span class="ef-req"> *</span></label>
        <div class="col-xs-12 col-sm-8 col-md-9">
            <input class="required form-control" type="email" name="gezag_e-mailadres" placeholder="E-mailadres" data-error="Geen geldig e-mailadres" required>
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <hr class="col-xs-12 no-padding hidden-sm">

    <div class="form-group">
        <label class="col-xs-12 col-sm-4 col-md-3"></label>
        <div class="col-xs-12 col-sm-8 col-md-9" class="inline-group">
            <div>
                <strong>Middels dit ingevulde aanmeldingsformulier meld ik me aan voor de
                    gekozen Magistrum opleiding. Tevens verklaar ik akkoord te gaan met de door mij
                    voor kennisgeving aangenomen bijgevoegde algemene voorwaarden.</strong>
            </div>
            <input type="checkbox" value=""/> Ik heb kennis genomen van de  algemene voorwaarden en ga hiermee akkoord.
        </div>
    </div>
    <div class="form-group ef-buttons">
        <div class="col-md-offset-3 col-sm-offset-4">
            <input type="submit" id="submit_button" name="submit" value="Verzenden" />
        </div>
    </div>
</form>