<?php get_header(); ?>

    <!-- Start Page Content Section -->
    <section class="page news-events-detail row-fluid">
        <div class="container">


            <!-- Start col-md-8 -->
            <div class="col-xs-12 col-sm-12 col-md-8 page-content news-content no-padding-right no-padding-xs margin-bottom-30">
                <?php if(have_posts()): while(have_posts()): the_post(); ?>

                    <h1 class="pagetitle col-xs-12 col-lg-12 no-padding"><?php the_title(); ?></h1>

                    <div class="meta meta-news col-xs-12 no-padding">
                        Geplaatst door: <span><?php the_author(); ?></span> op <span><?php echo get_the_date(); ?></span>
                    </div>

                    <?php the_post_thumbnail(); ?>
                    <?php the_content(); ?>

                <?php endwhile; endif; ?>
            </div>
            <aside class="col-xs-12 col-sm-12 col-md-3 pull-right sidebar no-padding-xs">
                <?php if(is_active_sidebar('io_contact_widget')) :
                    dynamic_sidebar('io_contact_widget');
                endif; ?>
            </aside>
        </div>
    </section>
<?php get_footer(); ?>