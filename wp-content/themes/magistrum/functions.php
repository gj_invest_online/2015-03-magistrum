<?php

// ********************************************************************************************************

if ( ! function_exists( 'io__setup' ) ) :
/**
* Sets up theme defaults and registers support for various WordPress features.
*
* Note that this function is hooked into the after_setup_theme hook, which
* runs before the init hook. The init hook is too late for some features, such
* as indicating support for post thumbnails.
*/
    function io_setup() {

        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'html5', array(
        	'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ));

        add_theme_support( 'post-formats', array(
        	'video'
        ));
		
		add_post_type_support( 'magazine', 'post-formats');

    }
endif; // io__setup

add_action( 'after_setup_theme', 'io_setup' );


// ********************************************************************************************************


// Replaces the excerpt "more" text by a link
add_filter('excerpt_more', 'new_excerpt_more');

function new_excerpt_more($more) {
	$read_more = '';	
			
	return $read_more;
}
// Custom excerpt length for FAQ and Testimonial Post type
//add_filter('excerpt_length', 'excerpt_length_per_post_type', 999);

function excerpt_length_per_post_type( $length ) {
	global $post;
	
    if ($post->post_type == 'faq') {
    	return 28;
    } elseif($post->post_type == 'testimonial') {
        return 12;
    }
}

// This code anbles Shortcodes in WordPress Text Widget
add_filter('widget_text', 'do_shortcode');

// define shortcode [social_media]
function get_social_media_icon($atts) {
    $icon_list = '<ul class="social">';

    foreach($atts as $media) {
        if($media == 'facebook') {
            $icon_list .= '<li><a href="#"><i class="fa fa-facebook social-icon"></i></a></li>';
        }
        elseif($media == 'twitter') {
            $icon_list .= '<li><a href="#"><i class="fa fa-twitter social-icon"></i></a></li>';
        }
        elseif($media == 'youtube') {
            $icon_list .= '<li><a href="#"><i class="fa fa-youtube social-icon"></i></a></li>';
        }
        elseif($media == 'linkedin') {
            $icon_list .= '<li><a href="#"><i class="fa fa-linkedin"></i></a></li>';
        }
        else {
            $icon_list .= '<li><a href="#"><i class="fa fa-question"></i></a></li>';
        }
    }

    $icon_list .= '</ul>';
    return $icon_list;
}

// register shortcode
add_shortcode( 'social_media', 'get_social_media_icon' );


require get_template_directory() . '/inc/scripts.php';
require get_template_directory() . '/inc/options.php';
require get_template_directory() . '/inc/navigation.php';
require get_template_directory() . '/inc/excerpts.php';
require get_template_directory() . '/inc/custom-post-types.php';
require get_template_directory() . '/inc/register-sidebar.php';
require get_template_directory() . '/inc/class-recent-posts-widget.php';
require get_template_directory() . '/inc/class-contact-widget.php';
require get_template_directory() . '/inc/class-faq-widget.php';
require get_template_directory() . '/inc/class-cta-widget.php';
?>