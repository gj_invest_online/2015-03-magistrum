<?php get_header(); ?>

<section class="page faq-overview row-fluid">	
	<div class="container">
		<h1 class="pagetitle col-xs-12 no-padding">
			Veelgestelde vragen
		</h1>
						
		<!-- Start col-md-8 -->
		<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-left no-padding-xs margin-bottom-30">
			<?php
			$args = array(
				'post_type' => 'faq',
				'posts_per_page' => 6,
				'orderby' => 'rand'
			);
			$faq = new WP_Query($args);
			
			if($faq->have_posts()): ?>
			
				<div class="row faq-popular">
					<div class="faq-category">
						<h2 class="hidden-xs">Meest gestelde vragen</h2>
						<ul class="col-xs-12 no-padding">
							
							<?php 
							while($faq->have_posts()): $faq->the_post();
								
								$terms = get_the_terms(get_the_ID(), 'onderwerp');
							
								if ( $terms && ! is_wp_error( $terms ) ) {
									foreach($terms as $term) {
										if($term->name) {
											$cat_name = $term->name;
										}
									}
								} else {
									$cat_name = 'vraag';
								}
								
								?>
								<li class="col-md-6 col-lg-4">
									<a href="<?php the_permalink(); ?>">
										<h3><?php the_title(); ?></h3>
										<?php echo excerpt_limit(20, '..'); ?>
										<label class="cta-grey"><?php echo $cat_name; ?></label>
										<i class="blue fa fa-angle-right"></i>										
									</a>
									
								</li>
														
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							
						</ul>
					</div>						
				</div>
			
			<?php endif; ?>
			
			<hr class="col-xs-12 no-padding" />
			
			<div class="row faq-categories">
			
				<h3>Alle categorieen</h3>
				<!-- Start panel group -->
				<div class="panel-group" id="questions" role="tablist" aria-multiselectable="true">
					<?php
					$faq_categories = get_object_taxonomies('faq');
					
					if(count($faq_categories) > 0) :
						
						$args = array (
							'orderby' => 'name',
							'show_count' => 0,
							'pad_counts' => 0,
							'hierarchical' => 1,
							'taxonomy' => 'onderwerp',
							'title_li' => ''
						);
						
						$cats = get_categories( $args );
						$counter = 1;
						
						foreach($cats as $cat) :
							
							$questions = get_posts(array (
								'showposts' => -1,
							    'post_type' => 'faq',
							    'tax_query' => array (
							        array (
								        'taxonomy' => 'onderwerp',
								        'field' => 'term_id',
								        'terms' => $cat->term_id
							    	),
								    'orderby' => 'title',
								    'order' => 'ASC')
							));
							
							
							?>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_<?php echo $counter; ?>">
							    	<h4 class="panel-title">
							             <a data-toggle="collapse" data-parent="#questions" href="#<?php echo $counter; ?>" aria-expanded="true" aria-controls="collapse_<?php echo $counter; ?>" class="collapsed">
							             <?php echo $cat->name; ?> (<?php echo $cat->count; ?>)
							             <small>Toon vragen</small>          
							             </a>
							         </h4>
								</div>
							    <div id="<?php echo $counter; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $counter; ?>" style="height: 0px;">
							    	<div class="panel-body">
							    		<ul class="col-xs-12 no-padding">
							    		<?php foreach ($questions as  $question) : ?>
							    			<li>
												<a href="<?php echo get_permalink($question->ID); ?>"><?php echo $question->post_title; ?> <i class="fa fa-angle-right"></i></a>
												<p><?php echo $question->post_content; ?></p>
											</li>	
							    		<?php endforeach; ?>
										</ul>	
									</div>
								</div>
							</div>
							<?php $counter++; ?>
						<?php endforeach;
						
					endif; ?>																									
					
				</div>
				<!-- End panel group -->
													
			<!-- End Row Categories -->	
			</div>
		<!-- End col-md-8 -->	
		</div>
		

		<aside class="col-xs-12 col-sm-12 col-md-3 pull-right sidebar no-padding-xs  no-padding-right">

			<?php if ( is_active_sidebar( 'course_widget_links' ) ) : ?>
				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<?php dynamic_sidebar( 'course_widget_links' ); ?>
				</div>
			<?php endif; ?>

			<?php if ( is_active_sidebar( 'io_contact_widget' ) ) : ?>
				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<?php dynamic_sidebar( 'io_contact_widget' ); ?>
				</div>
			<?php endif; ?>

		</aside>


	<!-- ./ End Main Container -->	
	</div>
<!-- ./ End Section page content -->		
</section>

<?php get_footer(); ?>