<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<title><?php wp_title(); ?></title>

	<!--== Include Fonts ==-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
	<?php wp_head(); ?>
</head>

<body>

<!-- HEADER -->
<header>
	<div class="container">

		<!-- NAVIGATION -->
		<nav class="row navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainmenu">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo bloginfo('url'); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Magistrum" class="img-responsive" />
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->

				<div class="collapse navbar-collapse" id="mainmenu">
					<ul class="nav navbar-nav navbar-right navbar-main">
						<?php mg_main_menu_items(); ?>
					</ul>

					<ul class="nav navbar-nav navbar-right navbar-sub">
						<?php mg_sub_menu_items() ?>
					</ul>

				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<!-- ./ End Navigation -->

	</div>
<!-- ./ End Header -->
</header>

<?php if( ! is_front_page()) : ?>

	<!-- Breadcrumbs -->
	<section class="breadcrumbs">
		<div class="container">

			<?php if ( function_exists('yoast_breadcrumb') ) :
				yoast_breadcrumb();
			endif; ?>

			<a class="btn-search"><i class="fa fa-search"></i></a>
		</div>
	</section>

	<?php get_search_form(); ?>

<?php endif; ?>
