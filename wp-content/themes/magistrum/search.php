<?php get_header(); ?>

<!-- Start Page Content Section -->
	<section class="page search-result row-fluid">
		<div class="container">

			<h1 class="pagetitle col-xs-12 no-padding"><i class="fa fa-search"></i> U heeft gezocht op: <?php echo get_search_query(); ?></h1>
			<?php get_template_part('templates/searchform', 'alternate'); ?>

			<div class="col-xs-12 col-sm-12 col-md-8 page-content no-padding-left no-padding-xs margin-bottom-30">

				<span class="search-meta col-xs-12 col-sm-11 col-md-11 no-padding">Uw zoekopdracht heeft <?php echo $wp_query->found_posts ?> resultaten opgeleverd</span>

				<?php if(have_posts()): while(have_posts()): the_post() ?>

				<article class="col-xs-12 col-sm-12 col-md-11 no-padding margin-bottom-40 searched-item">

					<figure class="col-md-3 col-sm-3 hidden-xs">
						<?php the_post_thumbnail(); ?>
					</figure>
					<div class="col-xs-12 col-sm-9 col-md-9 no-padding">

						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php the_excerpt(); ?>

					</div>
				</article>

				<?php endwhile; ?>
				<?php endif; ?>

			</div>

			<aside class="col-xs-12 col-sm-12 col-md-4 pull-right sidebar no-padding-xs no-padding-right">
				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<div class="widget widget-default widget-archive">
						<span class="widget-heading">Archief</span>
						<ul>
							<li>
								<i class="fa fa-caret-right"></i>&nbsp;Januari 2015
								<small class="pull-right">Archief tonen&nbsp;<i class="fa fa-angle-right"></i></small>
							</li>
							<li>
								<i class="fa fa-caret-right"></i>&nbsp;December 2014
								<small class="pull-right">Archief tonen&nbsp;<i class="fa fa-angle-right"></i></small>
							</li>
							<li>
								<i class="fa fa-caret-right"></i>&nbsp;November 2014
								<small class="pull-right">Archief tonen&nbsp;<i class="fa fa-angle-right"></i></small>
							</li>
							<li>
								<i class="fa fa-caret-right"></i>&nbsp;Oktober 2014
								<small class="pull-right">Archief tonen&nbsp;<i class="fa fa-angle-right"></i></small>
							</li>
						</ul>
					</div>
				</div>
				<!-- End Widget -->

				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<div class="widget widget-default widget-news">

						<span class="widget-heading">Nieuws & Events</span>
						<article>
							<div class="mask">
								<img src="http://www.avans.nl/binaries/content/gallery/nextweb/nieuws/scholarships.jpg" alt="title" />
							</div>
							<div class="msg">
								<span class="meta">Nieuws, 09 December 2014</span>
								<a class="title">17 buitenlandse studenten ontvangen Avans Scholarship</a>
							</div>
						</article>
						<article>
							<div class="mask">
								<img src="http://www.avans.nl/binaries/content/gallery/nextweb/nieuws/airplane_old_method.jpg" alt="title" />
							</div>
							<div class="msg">
								<span class="meta">Nieuws, 09 December 2014</span>
								<a class="title">Avans werkt mee aan verfverwijdertechniek luchtvaart</a>
							</div>
						</article>
						<article>
							<div class="mask">
								<img src="http://www.avans.nl/binaries/content/gallery/nextweb/nieuws/scholarships.jpg" alt="title" />
							</div>
							<div class="msg">
								<span class="meta">Nieuws, 09 December 2014</span>
								<a class="title">17 buitenlandse studenten ontvangen Avans Scholarship</a>
							</div>
						</article>
						<article>
							<div class="mask">
								<img src="http://www.avans.nl/binaries/content/gallery/nextweb/nieuws/scholarships.jpg" alt="title" />
							</div>
							<div class="msg">
								<span class="meta">Nieuws, 09 December 2014</span>
								<a class="title">17 buitenlandse studenten ontvangen Avans Scholarship</a>
							</div>
						</article>

						<a href="#" class="more-link">
							Meer Nieuws & Evenementen <i class="fa fa-angle-right"></i>
							<hr/>
						</a>
					</div>
				</div>
				<!-- End Widget -->

				<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
					<div class="widget widget-default widget-banner">
						<img src="assets/images/banner-sidebar-open-dag.jpg" alt="Open Dag Avans Deeltijd" />
					</div>
				</div>

			</aside>
			
		</div><!-- ./ End Main Container -->
	</section><!-- ./ End Section page content -->

<?php get_footer(); ?>