<?php get_header(); ?>

	<!-- Start Page Content Section -->
	<section class="page news-events row-fluid">
		<div class="container">

			<!-- Start col-md-8 -->
			<div class="col-xs-12 col-sm-12 col-md-7 page-content no-padding-left no-padding-xs margin-bottom-30">
				<h1 class="pagetitle col-xs-12 no-padding">Nieuws</h1>

				<?php if(have_posts()): while(have_posts()): the_post(); ?>

					<article class="col-xs-12 no-padding margin-bottom-40 news-item">
						<figure class="col-xs-12 col-sm-3 col-md-2 col-lg-3">
							<?php the_post_thumbnail(); ?>
						</figure>
						<div class="body col-xs-12 col-sm-9 col-md-9 col-lg-9">
							<h2>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								<small class="meta">27 januari 2015, door Avans Deeltijd</small>
							</h2>
							<?php the_excerpt(); ?>
						</div>
					</article>

				<?php endwhile; ?>
				<?php endif; ?>

			</div>

		</div>
	</section>
<?php get_footer(); ?>