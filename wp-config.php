<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'magistrum');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U71&Tnf8_?{{+:g*nhT {8=o>z[G`tD:+@wywPaX5AzcJIz3X&|cP)0 X6#t~WY|');
define('SECURE_AUTH_KEY',  'o=#cZ;H>Zf_MIO>~`._V?#6;ycrv4*?YuwkF  b+$*^8G1fH;+guS6E<2+Lu/!;x');
define('LOGGED_IN_KEY',    '/10~]lqMPQ[b@/l{$%`|}U-I9~|@84-`1Z(|`ltz@l:vRypk&fx<eaFi]4,S!K4-');
define('NONCE_KEY',        'z<U1%C|ZQ#w=:G|<[PK#fzN_S,%f/6mMBQij;I[WfF`.M b/93R X>Yf}i>%-6{u');
define('AUTH_SALT',        'eb{:8@}vJ6QT4@ctqy&y!Y)~[uQIMAZZV:geM;5xLy|P8l?mA{t&Ko4.-+>;<IQn');
define('SECURE_AUTH_SALT', 'Vx;S:#Q#y!glykz.Fo5CHFcHn/gU=MhJk=a~RGA)AnFZJXQCk-PbnREtV50wS2A$');
define('LOGGED_IN_SALT',   'mP*{[j=T%YNhS,>4FkLbHS|)u)S:RzW|9 +wjS-63Qpue19W<1qoYX2 L7g7C;*4');
define('NONCE_SALT',       '({0v@n--Wd#(08h<H7Nbt8?,8f*|<U)M*3e+ASy9bEuKICOfnoh%B?%_s%dymA+$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
